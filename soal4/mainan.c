#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <stdbool.h>


int main(int argc, char * argv[]) {
  if (argc != 5) {
    printf("Error: Wrong number of arguments");
    return 1;

  }

  if (strcmp(argv[1], "*") != 0 && (atoi(argv[1]) < 0 || atoi(argv[1]) > 23)) {
    printf("Error: Wrong hour argument");
    return 1;
  }

  if (strcmp(argv[2], "*") != 0 && (atoi(argv[2]) < 0 || atoi(argv[2]) > 59)) {
    printf("Error: Wrong minute argument");
    return 1;
  }

  if (strcmp(argv[3], "*") != 0 && (atoi(argv[3]) < 0 || atoi(argv[3]) > 59)) {
    printf("Error: Wrong second argument");
    return 1;
  }

  if (access(argv[4], F_OK) == -1) {
    printf("Error: File does not exist");
    return 1;
  }

  time_t t = time(NULL);
  struct tm tm = * localtime( & t);
  int curr_h = tm.tm_hour;
  int curr_m = tm.tm_min;
  int curr_s = tm.tm_sec;

  int hour = atoi(argv[1]);
  int minute = atoi(argv[2]);
  int second = atoi(argv[3]);

  if (strcmp(argv[1], "*") == 0) {
    hour = -1;
  }
  if (strcmp(argv[2], "*") == 0) {
    minute = -1;
  }
  if (strcmp(argv[3], "*") == 0) {
    second = -1;
  }

  int firstSleep = 0;

  if (hour == -1) {
    if (minute == -1) {
      if (second >= 0) {
        if (second < curr_s) {
          firstSleep = (second + 60) - curr_s;
        } else {
          firstSleep = second - curr_s;
        }
      } else {
        firstSleep = 1;
      }
    } else {
      firstSleep = (((minute * 60) + second) - ((curr_m * 60) + curr_s));
      if (minute < curr_m || (minute == curr_m && second < curr_s)) {
        firstSleep += 3600;
      }
    }
  } else {
    firstSleep = (((hour * 3600) + (minute * 60) + second) - ((curr_h * 3600) + (curr_m * 60) + curr_s));
    if (hour < curr_h || (hour == curr_h && (minute < curr_m || (minute == curr_m && second < curr_s)))) {
      firstSleep += 86400;
    }
  }

  if (hour == -1 && minute == -1 && second == -1) {
    firstSleep = 1;
  }

  bool first = true;

  pid_t pid = fork();
  if (pid < 0) {
    exit(EXIT_FAILURE);
  }

  if (pid > 0) {
    exit(EXIT_SUCCESS);
  }

  if (setsid() < 0) {
    exit(EXIT_FAILURE);
  }

  close(STDIN_FILENO);
  close(STDOUT_FILENO);
  close(STDERR_FILENO);

  while (1) {

    int sleepTime = 0;
    if (hour == -1 && minute == -1 && second == -1) {
      sleepTime = 1;
    }
    if (hour != -1) {
      sleepTime += 86400;
    } else if (minute != -1) {
      sleepTime += 3600;
    } else if (second != -1) {
      sleepTime += 60;
    }

    if (first) {
      sleep(firstSleep);
      first = false;

      if (hour == -1) {
        if (minute == -1) {
          if (second == -1) {
            sleepTime = 1;
          }
        } else {
          if (second == -1) {
            sleepTime -= curr_s;
          }
        }

      } else {
        if (minute == -1 && second == -1) {
          sleepTime -= ((curr_m * 60) + curr_s);
        } else if (minute == -1 && second >= 0) {
          sleepTime -= (curr_m * 60);
        } else if (minute >= 0 && second == -1) {
          sleepTime -= curr_s;
        }

      }

    }

    pid_t cid = fork();

    if (cid == -1) {
      printf("Error : failed to fork proccess\n");
    } else if (cid == 0) {
      char * args[] = {
        "bash",
        argv[4],
        NULL
      };
      execv("/bin/bash", args);
    } else {
      wait(NULL);
    }

    sleep(sleepTime);

  }

  return 0;

}
