#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <time.h>
#include <sys/wait.h>
#include <string.h>
#include <sys/stat.h>
#include <signal.h>

char dir_name[20];

void zip_folder(char* zip_name);
void hapus_folder();
void killer(pid_t pid, char param);

void download_img(){
    
    char file_name[100];

    //digunakan untuk penamaan img
    time_t now = time(NULL);
    strftime(file_name, 20, "%Y-%m-%d_%H:%M:%S", localtime(&now));

    //penamaan img
    char name_img[150];
    sprintf(name_img, "%s.jpeg", file_name);

    //mengambil t (waktu dalam second)
    time_t t;
    t = time(NULL);

    //menetapkan ukuran
    int ukuran = (t % 1000) + 50;

    //mengambil link url dengan ukuran yang telah disesuaikan
    char link_img[100];
    sprintf(link_img, "https://picsum.photos/%d", ukuran);

    if(fork() == 0){
        //akan download gambar
        char *gmbr[5] = {"wget", "-qO", name_img, link_img, NULL};
        execv("/usr/bin/wget", gmbr);
    }
}


void make_dir(){

    // membuat directory baru 
    char *argvp[3] = {"mkdir", dir_name, NULL};
    execvp("mkdir", argvp);
}


int main(int argc, char *argv[]){

    if(argc != 2 || argv[1][1] != 'a' && argv[1][1] != 'b'){
        printf("Argumen tidak tepat bro");
        return 0;
    }


    pid_t pid, sid;

    //membuat parent id
    pid = fork();

    if(pid < 0){
        exit(EXIT_FAILURE);
    }

    if(pid > 0){
        exit(EXIT_SUCCESS);
    }

    killer(getpid(), argv[1][1]);
    umask(0);

    sid = setsid();

    if(sid < 0){
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    killer(getpid(), argv[1][1]);
    while(1){

        //get time sekarang untuk penamaan directory
        time_t now = time(NULL);
        strftime(dir_name, 20, "%Y-%m-%d_%H:%M:%S", localtime(&now));


        pid_t id;
        id = fork();

        //membuat directory setiap 30 detik
        if (id == 0){ //child process
            make_dir(dir_name);
            // exit(0);
        }

        //menunggu pembuatan directory selama 1 detik
        sleep(1);

        pid_t pid2;
        pid2 = fork();

        //terjadi download image dan mengubah current directory menjadi zip setelah download 15 gambar
        if(pid2 == 0) { //parent process

            chdir(dir_name);

            for(int i = 1; i <= 15; i++){
                download_img();
                sleep(5);
            }
            
            chdir("..");
            char zip_name[100];
            sprintf(zip_name, "%s.zip", dir_name);

            zip_folder(zip_name);
            hapus_folder();
              
            exit(0);
        }
        sleep(29);
    }
    return EXIT_SUCCESS;

}

void zip_folder(char* zip_name){
    pid_t pid;
    pid = fork();
    if (pid == 0)
    {
        char *argv[] = {"zip", "-r", zip_name, dir_name, NULL};
        execv("/bin/zip", argv);
        exit(0);
    } else if(pid>0){
        wait(NULL);
    }
}

void hapus_folder(){
    pid_t pid;
    pid=fork();

    if(pid==0){
        execlp("rm", "rm", "-r", dir_name, NULL);
        exit(0);
    }else if(pid>0){
        wait(NULL);
        printf("menghapus folder sukses!!\n");
    }
}

void killer(pid_t pid, char param){
    char cwd[100];
    //mendapatkan direktori saat ini
    getcwd(cwd, sizeof(cwd));
    int status;
    int test;
    //melakukan forking pada parent
    pid_t parent = fork();
    if (parent < 0) 
        exit(EXIT_FAILURE);

    if (parent == 0) {
        FILE *fp;
        char killer_file[200];
        char data[1000];
        char dest[200];
        char file_name[200];
        char mode[100];
        
        sprintf(file_name, "%s/killer.c", cwd);
        sprintf(dest, "%s/killer", cwd);

        sprintf(killer_file, "%s/killer.c", cwd);

        if (param == 'a') 
          strcpy(mode, " char *argv[] = {\"killall\", \"-s\", \"9\", \"lukisan\", NULL}; execv(\"/usr/bin/killall\", argv);");
        else if (param == 'b') 
          sprintf(mode, " char *argv[] = {\"kill\", \"%d\", NULL}; execv(\"/usr/bin/kill\", argv);", pid);
        
        sprintf(data,   "#include <stdlib.h>\n"
                        "#include <unistd.h>\n"
                        "#include <stdio.h>\n"
                        "#include <wait.h>\n"
                        "#include <string.h>\n"
                        "#include <sys/types.h>\n"

                        " int main() {"
                        " pid_t child_id;"
                        " int status;"

                        " child_id = fork();"
                        " if (child_id < 0) exit(EXIT_FAILURE);"

                        " if (child_id == 0) {"
                            " %s"
                        " } else {"
                            " while (wait(&status)>0);"
                            " char *argv[] = {\"rm\", \"%s/killer\", NULL};"
                            " execv(\"/bin/rm\", argv);"
                        " }"
                        " }", mode, cwd);

        fp = fopen(killer_file, "w");
        fputs(data, fp);
        fclose(fp);

        char *argv[] = {"gcc", file_name, "-o", dest, NULL};
        execv("/usr/bin/gcc", argv);
    }
    
    while(wait(&status) > 0);
    //melakukan forking pada remove_killer
    pid_t remove_killer = fork();
    if (remove_killer < 0) 
      exit(EXIT_FAILURE);

    if (remove_killer == 0) {
        char src[200];
        sprintf(src, "%s/killer.c", cwd);
        char *argv[] = {"rm", src, NULL};
        execv("/bin/rm", argv);
    }
    
    while(wait(&status) > 0);
}
