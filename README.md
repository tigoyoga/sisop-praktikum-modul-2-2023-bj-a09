# sisop-praktikum-modul-2-2023-BJ-A09

Perkenalkan kami dari kelas sistem operasi kelompok A09, dengan anggota sebagai berikut:


|Nama                   |NRP        |
|---|---|
|Muhammad Rifqi Fadhilah|5025211228 |
|Muhammad Naufal Baihaqi|5025211103 |
|Tigo S Yoga            |5025211125 |

# Nomor 1

Grape-kun adalah seorang penjaga hewan di kebun binatang, dia mendapatkan tugas dari atasannya untuk melakukan penjagaan pada beberapa hewan-hewan yang ada di kebun binatang sebelum melakukan penjagaan Grape-kun harus mengetahui terlebih dahulu hewan apa aja yang harus dijaga dalam drive kebun binatang tersebut terdapat folder gambar dari hewan apa saja yang harus dijaga oleh Grape-kun. Berikut merupakan link download dari drive kebun binatang tersebut : https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq 

a. Grape-kun harus mendownload file tersebut untuk disimpan pada penyimpanan local komputernya. Dan untuk melakukan melihat file gambar pada folder yang telah didownload Grape-kun harus melakukan unzip pada folder tersebut.

b. Setelah berhasil melakukan unzip Grape-kun melakukan pemilihan secara acak pada file gambar tersebut untuk melakukan shift penjagaan pada hewan tersebut.

c. Karena Grape-kun adalah orang yang perfeksionis Grape-kun ingin membuat direktori untuk memilah file gambar tersebut. Direktori tersebut dengan nama HewanDarat, HewanAmphibi, dan HewanAir. Setelah membuat direktori tersebut Grape-kun harus melakukan filter atau pemindahan file gambar hewan sesuai dengan tempat tinggal nya.

d. Setelah mengetahui hewan apa saja yang harus dijaga Grape-kun melakukan zip kepada direktori yang dia buat sebelumnya agar menghemat penyimpanan.

Catatan : 
untuk melakukan zip dan unzip tidak boleh menggunakan system

Jawaban :

```
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <string.h>

void makeDirectories();
void moveFiles();
void compressFiles();
void clearProcesses();
void downloadFiles();
void shiftAnimal();

int main()
{
    downloadFiles();
    shiftAnimal();
    makeDirectories();
    moveFiles();
    compressFiles();
    clearProcesses();
    return 0;
}

void makeDirectories()
{
    pid_t pid;
    pid = fork();
    if(pid==0){
        char *argv[] = {"mkdir", "-p", "HewanDarat", "HewanAir", "HewanAmphibi", NULL};
        execv("/bin/mkdir", argv);
        exit(0);
    }else if(pid>0){
        wait(NULL);
        printf("all directory success created\n");
    }
}

void shiftAnimal()
{
    char command[256];
    snprintf(command, sizeof(command), "/usr/bin/shuf -n 1 *.jpg");
    system(command);
}

void moveFiles(){
    system("mv *air.jpg HewanAir");
    system("mv *darat.jpg HewanDarat");
    system("mv *amphibi.jpg HewanAmphibi");
}

void compressFiles()
{
    pid_t pid;
    pid = fork();
    if(pid==0)
    {
        char *argv[] = {"zip","-r", "HewanAir.zip", "HewanAir", NULL};
        execv("/bin/zip", argv);
        exit(0);
    }
    else if(pid>0)
    {
        wait(NULL);
        pid = fork();
        if (pid==0)
        {
            char *argv[] = {"zip", "-r", "HewanDarat.zip", "HewanDarat", NULL};
            execv("/bin/zip", argv);
            exit(0);
        }
        else if(pid>0)
        {
            wait(NULL);
            pid = fork();
            if (pid == 0)
            {
                char *argv[] = {"zip", "-r", "HewanAmphibi.zip", "HewanAmphibi", NULL};
                execv("/bin/zip", argv);
                exit(0);
            } else if(pid>0){
                wait(NULL);
                printf("zip all directories success\n");
            }
        }
    }
}

void clearProcesses()
{
    system("rm -rf HewanDarat");
    system("rm -rf HewanAir");
    system("rm -rf HewanAmphibi");
    printf("all process are cleared!");
}

void downloadFiles()
{
    pid_t pid ;

    pid = fork();

    if(pid==0){
        char *argv[] = {"wget", "https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq", "-O", "binatang.zip", NULL};
        execv("/usr/bin/wget", argv);
        exit(0);
    }else if(pid>0){
        wait(NULL);

        pid = fork();
        if(pid==0){
            char *argv[] = {"unzip", "binatang.zip", NULL};
            execv("/bin/unzip", argv);
            exit(0);
        }else if(pid>0){
            wait(NULL);
            printf("DOWNLOAD AND UNZIPPP SUCCESS\n");
        }
    }
    
    
}
```

### 1. berikut merupakan header file yang diperlukan dalam program 

```
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <string.h>
```
- `#include <stdio.h>` Header ini berisi definisi untuk fungsi input-output standar seperti printf(), scanf(), dan sebagainya.

- `#include <stdlib.h>` Header ini berisi definisi untuk fungsi-fungsi standar yang digunakan dalam program C, seperti alokasi memori (malloc() dan free()), konversi bilangan, dan fungsi pengurutan.

- `#include <sys/types.h>` Header ini berisi definisi untuk tipe data khusus, seperti pid_t (untuk menyimpan ID proses), size_t (untuk menyimpan ukuran memori), dan lainnya.

- `#include <unistd.h>` Header ini berisi definisi untuk fungsi-fungsi yang digunakan untuk mengakses sistem operasi, seperti fork() (untuk membuat proses baru), exec() (untuk mengeksekusi perintah shell), dan lainnya.

- `#include <sys/wait.h>` Header ini berisi definisi untuk fungsi-fungsi yang digunakan untuk menunggu proses anak selesai, seperti wait() dan waitpid().

- `#include <sys/stat.h>` Header ini berisi definisi untuk struktur stat, yang berisi informasi tentang sebuah file (ukuran, waktu modifikasi, dan lain-lain).

- `#include <string.h>` Header ini berisi definisi untuk fungsi-fungsi pemrosesan string, seperti strcpy() (untuk menyalin string), strlen() (untuk menghitung panjang string), dan lainnya.

### 2. berikut adalah kode fungsi makeDirectories()

```
void makeDirectories()
{
    pid_t pid;
    pid = fork();
    if(pid==0){
        char *argv[] = {"mkdir", "-p", "HewanDarat", "HewanAir", "HewanAmphibi", NULL};
        execv("/bin/mkdir", argv);
        exit(0);
    }else if(pid>0){
        wait(NULL);
        printf("all directory success created\n");
    }
}

```

Fungsi makeDirectories() bertujuan untuk membuat tiga direktori baru pada direktori kerja saat ini. Direktori-direktori tersebut adalah HewanDarat, HewanAir, dan HewanAmphibi.

Pertama-tama, fungsi ini membuat proses baru menggunakan fork(). Proses baru akan menjalankan perintah mkdir untuk membuat ketiga direktori tersebut. Perintah ini dijalankan melalui fungsi execv() yang akan mengeksekusi perintah mkdir dan argumen-argumennya.

Jika proses baru berhasil dibuat, maka kontrol akan kembali ke proses utama dan menunggu proses anak selesai dieksekusi menggunakan wait(). Setelah proses anak selesai dieksekusi, maka pesan "all directory success created" akan ditampilkan di konsol.

Jika proses pembuatan direktori gagal dilakukan, maka akan terjadi error dan program akan keluar dengan status error.

### 3. berikut adalah kode fungsi shiftAnimal()

```
void shiftAnimal()
{
    char command[256];
    snprintf(command, sizeof(command), "/usr/bin/shuf -n 1 *.jpg");
    system(command);
}

```

Fungsi shiftAnimal() digunakan untuk mengacak file gambar hewan dengan ekstensi ".jpg" yang berada pada direktori kerja saat program dijalankan, dan menampilkan satu file secara acak.

Pertama, fungsi membuat sebuah array karakter command dengan ukuran 256 byte. Kemudian, snprintf() digunakan untuk mengisi array command dengan perintah untuk menjalankan perintah shell "shuf" yang dijalankan dengan argumen "-n 1 .jpg". Argumen "-n 1" akan membatasi output dari perintah "shuf" menjadi satu baris saja, sedangkan ".jpg" akan memilih semua file dengan ekstensi ".jpg" yang berada pada direktori kerja saat program dijalankan.

Setelah itu, perintah shell dijalankan dengan memanggil system(command) dan mengirimkan argumen command sebagai input. Hasil output dari perintah shell akan langsung ditampilkan pada terminal.

### 4. berikut adalah kode fungsi moveFiles()

```
void moveFiles(){
    system("mv *air.jpg HewanAir");
    system("mv *darat.jpg HewanDarat");
    system("mv *amphibi.jpg HewanAmphibi");
}
```
Fungsi moveFiles() digunakan untuk memindahkan file gambar ke direktori yang sesuai dengan jenis hewan. Ada tiga jenis hewan yaitu HewanAir, HewanDarat, dan HewanAmphibi.

Pertama-tama, sistem menggunakan perintah mv untuk memindahkan semua file gambar yang memiliki ekstensi .jpg dengan nama yang mengandung kata kunci air ke direktori HewanAir. Begitu juga untuk file gambar dengan nama yang mengandung kata kunci darat dan amphibi yang dipindahkan ke direktori HewanDarat dan HewanAmphibi secara berurutan.

Fungsi system() digunakan untuk menjalankan perintah shell dalam program C dan mengembalikan nilai 0 jika perintah berhasil dijalankan. Oleh karena itu, tidak diperlukan nilai balik dari fungsi moveFiles()

### 5. berikut adalah kode fungsi compressFiles()

```
void compressFiles()
{
    pid_t pid;
    pid = fork();
    if(pid==0)
    {
        char *argv[] = {"zip","-r", "HewanAir.zip", "HewanAir", NULL};
        execv("/bin/zip", argv);
        exit(0);
    }
    else if(pid>0)
    {
        wait(NULL);
        pid = fork();
        if (pid==0)
        {
            char *argv[] = {"zip", "-r", "HewanDarat.zip", "HewanDarat", NULL};
            execv("/bin/zip", argv);
            exit(0);
        }
        else if(pid>0)
        {
            wait(NULL);
            pid = fork();
            if (pid == 0)
            {
                char *argv[] = {"zip", "-r", "HewanAmphibi.zip", "HewanAmphibi", NULL};
                execv("/bin/zip", argv);
                exit(0);
            } else if(pid>0){
                wait(NULL);
                printf("zip all directories success\n");
            }
        }
    }
}
```

Kode ini merupakan sebuah fungsi yang digunakan untuk melakukan kompresi file dari setiap direktori yang telah dibuat pada fungsi makeDirectories(). Proses kompresi dilakukan dengan menggunakan perintah zip pada terminal. Fungsi ini terdiri dari beberapa bagian, yaitu:

- Membuat proses dengan memanggil fungsi fork() dan menyimpan PID yang dihasilkan ke dalam variabel pid.
- Jika nilai pid adalah 0, artinya kita berada dalam proses child. Pada tahap ini, kita akan membuat sebuah array of string argv yang berisi perintah untuk melakukan kompresi file dengan zip, beserta argumen-argumennya. execv() digunakan untuk menjalankan perintah tersebut, dan menggantikan proses child yang sedang berjalan. Setelah itu, proses child akan diakhiri dengan memanggil exit(0).
- Jika nilai pid adalah lebih besar dari 0, artinya kita berada dalam proses parent. Kita akan menunggu hingga proses child selesai dijalankan dengan memanggil wait(NULL). Setelah itu, kita akan membuat proses baru lagi untuk melakukan kompresi pada direktori HewanDarat, dan menunggu hingga proses tersebut selesai dengan memanggil wait(NULL) lagi. Kemudian kita membuat proses untuk melakukan kompresi pada direktori HewanAmphibi, dan menunggu hingga proses tersebut selesai. Setelah proses terakhir selesai dijalankan, program akan mencetak pesan "zip all directories success".

### 6. berikut adalah kode fungsi clearProcesses()

```
void clearProcesses()
{
    system("rm -rf HewanDarat");
    system("rm -rf HewanAir");
    system("rm -rf HewanAmphibi");
    printf("all process are cleared!");
}
```

Fungsi clearProcesses() bertujuan untuk menghapus direktori-direktori yang telah dibuat sebelumnya, yaitu HewanDarat, HewanAir, dan HewanAmphibi. Fungsi ini dipanggil setelah proses kompresi file selesai dilakukan.

Pada implementasinya, fungsi ini menggunakan tiga kali pemanggilan sistem system() untuk menjalankan perintah rm -rf <nama direktori> untuk menghapus masing-masing direktori. Setelah proses penghapusan selesai dilakukan, fungsi akan mencetak pesan "all process are cleared!".

### 7. berikut adalah kode fungsi downloadFiles()

```
void downloadFiles()
{
    pid_t pid ;

    pid = fork();

    if(pid==0){
        char *argv[] = {"wget", "https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq", "-O", "binatang.zip", NULL};
        execv("/usr/bin/wget", argv);
        exit(0);
    }else if(pid>0){
        wait(NULL);

        pid = fork();
        if(pid==0){
            char *argv[] = {"unzip", "binatang.zip", NULL};
            execv("/bin/unzip", argv);
            exit(0);
        }else if(pid>0){
            wait(NULL);
            printf("DOWNLOAD AND UNZIPPP SUCCESS\n");
        }
    }
    
    
}
```

Code tersebut adalah implementasi dari sebuah fungsi bernama downloadFiles() yang digunakan untuk mendownload file binatang.zip dari sebuah URL menggunakan perintah wget dan mengekstrak file zip tersebut menggunakan perintah unzip.

Pertama-tama, fungsi ini melakukan proses fork() untuk membuat proses baru. Jika proses baru tersebut adalah proses child (pid==0), maka akan dijalankan perintah wget dengan menggunakan fungsi execv(). Perintah wget digunakan untuk mendownload file dari internet. Pada fungsi ini, file yang didownload adalah binatang.zip dari URL yang dihubungkan menggunakan https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq. Opsi -O digunakan untuk menentukan nama file yang akan diunduh. Setelah selesai, proses child akan diakhiri menggunakan perintah exit(0).

Jika proses baru tersebut adalah proses parent (pid>0), maka parent akan menunggu proses child selesai dengan menggunakan perintah wait(NULL). Setelah proses child selesai, proses parent akan melakukan proses fork() lagi untuk membuat proses baru yang akan mengekstrak file zip yang telah diunduh. Jika proses baru tersebut adalah proses child, maka akan dijalankan perintah unzip dengan menggunakan fungsi execv(). Perintah unzip digunakan untuk mengekstrak isi dari file zip yang telah diunduh. Pada fungsi ini, file zip yang akan diekstrak adalah binatang.zip. Setelah selesai, proses child akan diakhiri menggunakan perintah exit(0).

Jika proses baru tersebut adalah proses parent, maka parent akan menunggu proses child selesai dengan menggunakan perintah wait(NULL). Setelah proses child selesai, fungsi downloadFiles() akan mencetak pesan DOWNLOAD AND UNZIPPP SUCCESS untuk menandakan bahwa proses download dan ekstraksi file zip telah selesai.

Regenerate response

![soal1](https://user-images.githubusercontent.com/88433109/230728004-1ea5e6dd-21d1-4e09-9576-b132a7014c90.JPG)

# Nomor 2

Sucipto adalah seorang seniman terkenal yang berasal dari Indonesia. Karya nya sudah terkenal di seluruh dunia, dan lukisannya sudah dipajang di berbagai museum mancanegara. Tetapi, akhir-akhir ini sucipto sedang terkendala mengenai ide lukisan ia selanjutnya. Sebagai teman yang jago sisop, bantu sucipto untuk melukis dengan mencarikannya gambar-gambar di internet sebagai referensi !

a. Pertama-tama, buatlah sebuah folder khusus, yang dalamnya terdapat sebuah program C yang per 30 detik membuat sebuah folder dengan nama timestamp [YYYY-MM-dd_HH:mm:ss].

b. Tiap-tiap folder lalu diisi dengan 15 gambar yang di download dari https://picsum.photos/ , dimana tiap gambar di download setiap 5 detik. Tiap gambar berbentuk persegi dengan ukuran (t%1000)+50 piksel dimana t adalah detik Epoch Unix. Gambar tersebut diberi nama dengan format timestamp [YYYY-mm-dd_HH:mm:ss].

c. Agar rapi, setelah sebuah folder telah terisi oleh 15 gambar, folder akan di zip dan folder akan di delete(sehingga hanya menyisakan .zip).

d. Karena takut program tersebut lepas kendali, Sucipto ingin program tersebut men-generate sebuah program "killer" yang siap di run(executable) untuk menterminasi semua operasi program tersebut. Setelah di run, program yang menterminasi ini lalu akan mendelete dirinya sendiri.

e. Buatlah program utama bisa dirun dalam dua mode, yaitu MODE_A dan MODE_B. untuk mengaktifkan MODE_A, program harus dijalankan dengan argumen -a. Untuk MODE_B, program harus dijalankan dengan argumen -b. Ketika dijalankan dalam MODE_A, program utama akan langsung menghentikan semua operasinya ketika program killer dijalankan. Untuk MODE_B, ketika program killer dijalankan, program utama akan berhenti tapi membiarkan proses di setiap folder yang masih berjalan sampai selesai(semua folder terisi gambar, terzip lalu di delete).
	
  Catatan :
  - Tidak boleh menggunakan system()
  - Proses berjalan secara daemon
  - Proses download gambar pada beberapa folder dapat berjalan secara bersamaan (overlapping)

Jawaban :

```
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <time.h>
#include <sys/wait.h>
#include <string.h>
#include <sys/stat.h>
#include <signal.h>

char dir_name[20];

void zip_folder(char* zip_name);
void hapus_folder();
void killer(pid_t pid, char param);

void download_img(){
    
    char file_name[100];

    //digunakan untuk penamaan img
    time_t now = time(NULL);
    strftime(file_name, 20, "%Y-%m-%d_%H:%M:%S", localtime(&now));

    //penamaan img
    char name_img[150];
    sprintf(name_img, "%s.jpeg", file_name);

    //mengambil t (waktu dalam second)
    time_t t;
    t = time(NULL);

    //menetapkan ukuran
    int ukuran = (t % 1000) + 50;

    //mengambil link url dengan ukuran yang telah disesuaikan
    char link_img[100];
    sprintf(link_img, "https://picsum.photos/%d", ukuran);

    if(fork() == 0){
        //akan download gambar
        char *gmbr[5] = {"wget", "-qO", name_img, link_img, NULL};
        execv("/usr/bin/wget", gmbr);
    }
}


void make_dir(){

    // membuat directory baru 
    char *argvp[3] = {"mkdir", dir_name, NULL};
    execvp("mkdir", argvp);
}


int main(int argc, char *argv[]){

    if(argc != 2 || argv[1][1] != 'a' && argv[1][1] != 'b'){
        printf("Argumen tidak tepat bro");
        return 0;
    }


    pid_t pid, sid;

    //membuat parent id
    pid = fork();

    if(pid < 0){
        exit(EXIT_FAILURE);
    }

    if(pid > 0){
        exit(EXIT_SUCCESS);
    }

    killer(getpid(), argv[1][1]);
    umask(0);

    sid = setsid();

    if(sid < 0){
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    killer(getpid(), argv[1][1]);
    while(1){

        //get time sekarang untuk penamaan directory
        time_t now = time(NULL);
        strftime(dir_name, 20, "%Y-%m-%d_%H:%M:%S", localtime(&now));


        pid_t id;
        id = fork();

        //membuat directory setiap 30 detik
        if (id == 0){ //child process
            make_dir(dir_name);
            // exit(0);
        }

        //menunggu pembuatan directory selama 1 detik
        sleep(1);

        pid_t pid2;
        pid2 = fork();

        //terjadi download image dan mengubah current directory menjadi zip setelah download 15 gambar
        if(pid2 == 0) { //parent process

            chdir(dir_name);

            for(int i = 1; i <= 15; i++){
                download_img();
                sleep(5);
            }
            
            chdir("..");
            char zip_name[100];
            sprintf(zip_name, "%s.zip", dir_name);

            zip_folder(zip_name);
            hapus_folder();
              
            exit(0);
        }
        sleep(29);
    }
    return EXIT_SUCCESS;

}

void zip_folder(char* zip_name){
    pid_t pid;
    pid = fork();
    if (pid == 0)
    {
        char *argv[] = {"zip", "-r", zip_name, dir_name, NULL};
        execv("/bin/zip", argv);
        exit(0);
    } else if(pid>0){
        wait(NULL);
    }
}

void hapus_folder(){
    pid_t pid;
    pid=fork();

    if(pid==0){
        execlp("rm", "rm", "-r", dir_name, NULL);
        exit(0);
    }else if(pid>0){
        wait(NULL);
        printf("menghapus folder sukses!!\n");
    }
}

void killer(pid_t pid, char param){
    char cwd[100];
    //mendapatkan direktori saat ini
    getcwd(cwd, sizeof(cwd));
    int status;
    int test;
    //melakukan forking pada parent
    pid_t parent = fork();
    if (parent < 0) 
        exit(EXIT_FAILURE);

    if (parent == 0) {
        FILE *fp;
        char killer_file[200];
        char data[1000];
        char dest[200];
        char file_name[200];
        char mode[100];
        
        sprintf(file_name, "%s/killer.c", cwd);
        sprintf(dest, "%s/killer", cwd);

        sprintf(killer_file, "%s/killer.c", cwd);

        if (param == 'a') 
          strcpy(mode, " char *argv[] = {\"killall\", \"-s\", \"9\", \"lukisan\", NULL}; execv(\"/usr/bin/killall\", argv);");
        else if (param == 'b') 
          sprintf(mode, " char *argv[] = {\"kill\", \"%d\", NULL}; execv(\"/usr/bin/kill\", argv);", pid);
        
        sprintf(data,   "#include <stdlib.h>\n"
                        "#include <unistd.h>\n"
                        "#include <stdio.h>\n"
                        "#include <wait.h>\n"
                        "#include <string.h>\n"
                        "#include <sys/types.h>\n"

                        " int main() {"
                        " pid_t child_id;"
                        " int status;"

                        " child_id = fork();"
                        " if (child_id < 0) exit(EXIT_FAILURE);"

                        " if (child_id == 0) {"
                            " %s"
                        " } else {"
                            " while (wait(&status)>0);"
                            " char *argv[] = {\"rm\", \"%s/killer\", NULL};"
                            " execv(\"/bin/rm\", argv);"
                        " }"
                        " }", mode, cwd);

        fp = fopen(killer_file, "w");
        fputs(data, fp);
        fclose(fp);

        char *argv[] = {"gcc", file_name, "-o", dest, NULL};
        execv("/usr/bin/gcc", argv);
    }
    
    while(wait(&status) > 0);
    //melakukan forking pada remove_killer
    pid_t remove_killer = fork();
    if (remove_killer < 0) 
      exit(EXIT_FAILURE);

    if (remove_killer == 0) {
        char src[200];
        sprintf(src, "%s/killer.c", cwd);
        char *argv[] = {"rm", src, NULL};
        execv("/bin/rm", argv);
    }
    
    while(wait(&status) > 0);
}
```

### 1. berikut merupakan header file yand di perlukan didalam program

```
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <string.h>
```

- `#include <stdio.h>` Header ini digunakan untuk input dan output standar pada program, seperti printf dan scanf.

- `#include <stdlib.h>`Header ini berisi fungsi-fungsi yang berhubungan dengan alokasi dan dealokasi memori, konversi angka, fungsi pengurutan dan pencarian, dan fungsi-fungsi umum lainnya.

- `#include <sys/types.h>` Header ini berisi definisi untuk tipe data khusus seperti pid_t (untuk proses id), size_t (untuk ukuran), dan lainnya.

- `#include <unistd.h>` Header ini berisi fungsi-fungsi untuk melakukan interaksi dengan sistem operasi, seperti fork() dan exec().

- `#include <sys/wait.h>` Header ini berisi fungsi-fungsi untuk mengontrol proses, seperti wait(), waitpid(), dan lainnya.

- `#include <sys/stat.h>` Header ini berisi definisi untuk struktur stat yang digunakan untuk mengambil informasi tentang file dan direktori.

- `#include <string.h>` Header ini berisi fungsi-fungsi untuk manipulasi string, seperti strlen(), strcpy(), dan lainnya.

### 2. berikut adalah kode fungsi download_img()

```
void download_img(){
    
    char file_name[100];

    //digunakan untuk penamaan img
    time_t now = time(NULL);
    strftime(file_name, 20, "%Y-%m-%d_%H:%M:%S", localtime(&now));

    //penamaan img
    char name_img[150];
    sprintf(name_img, "%s.jpeg", file_name);

    //mengambil t (waktu dalam second)
    time_t t;
    t = time(NULL);

    //menetapkan ukuran
    int ukuran = (t % 1000) + 50;

    //mengambil link url dengan ukuran yang telah disesuaikan
    char link_img[100];
    sprintf(link_img, "https://picsum.photos/%d", ukuran);

    if(fork() == 0){
        //akan download gambar
        char *gmbr[5] = {"wget", "-qO", name_img, link_img, NULL};
        execv("/usr/bin/wget", gmbr);
    }
}

```

Code tersebut merupakan implementasi dari fungsi download_img(), yang memiliki tujuan untuk mengunduh gambar dari internet dengan ukuran yang telah ditentukan. Berikut ini adalah penjelasan detail dari setiap bagian code tersebut:

- `char file_name[100]`: mendeklarasikan variabel `file_name` dengan tipe data `char` dan kapasitas sebesar 100 byte.

- `time_t now = time(NULL);`: mendeklarasikan variabel `now` dengan tipe data `time_t` dan memberikan nilai dari fungsi `time(NULL)`, yaitu waktu saat ini dalam bentuk `time_t`.

`strftime(file_name, 20, "%Y-%m-%d_%H:%M:%S", localtime(&now));`: mengubah format waktu dari now menjadi string dalam format `"%Y-%m-%d_%H:%M:%S"` dan menyimpannya ke dalam variabel `file_name`.

`char name_img[150];`: mendeklarasikan variabel `name_img` dengan tipe data `char` dan kapasitas sebesar 150 byte.

`sprintf(name_img, "%s.jpeg", file_name);`: menyimpan nama gambar dengan format `<file_name>`.jpeg ke dalam variabel `name_img`, dimana `file_name` adalah string yang sudah dihasilkan pada langkah sebelumnya.

`time_t t; t = time(NULL);`: mendeklarasikan variabel `t` dengan tipe data `time_t` dan memberikan nilai dari fungsi `time(NULL)`, yaitu waktu saat ini dalam bentuk `time_t`.

`int ukuran = (t % 1000) + 50;`: mendeklarasikan variabel ukuran dengan tipe data `int` dan memberikan nilai yang dihasilkan dari operasi `(t % 1000) + 50`, yaitu nilai acak antara 50 hingga 1049.

`char link_img[100];`: mendeklarasikan variabel `link_img` dengan tipe data `char` dan kapasitas sebesar 100 byte.

`sprintf(link_img, "https://picsum.photos/%d", ukuran);`: menyimpan link URL dengan format `https://picsum.photos/<ukuran>` ke dalam variabel `link_img`, dimana `ukuran` adalah nilai yang sudah dihasilkan pada langkah sebelumnya.

`if(fork() == 0){...`: membuat proses child dengan fungsi `fork()`, dan menjalankan kode di dalam blok `if` pada proses child.

`char *gmbr[5] = {"wget", "-qO", name_img, link_img, NULL};`: mendeklarasikan array `gmbr` dengan tipe data `char*` dan panjang 5, dan memberikan nilai berupa array string yang terdiri dari `"wget"`, `"-qO"`, `name_img`, `link_img`, dan `NULL`.

`execv("/usr/bin/wget", gmbr);`: menjalankan perintah `wget` dengan parameter dari array `gmbr` menggunakan fungsi `execv()`.

### 3. berikut adalah kode fungsi make_dir()

```
void make_dir(){

    // membuat directory baru 
    char *argvp[3] = {"mkdir", dir_name, NULL};
    execvp("mkdir", argvp);
}
```
Fungsi `make_dir()` digunakan untuk membuat sebuah direktori baru pada sistem operasi dengan memanfaatkan perintah `mkdir`. Direktori yang dibuat memiliki nama yang disimpan dalam variabel `dir_name`.

Pada fungsi ini, pertama-tama dibuat sebuah array `argvp ` yang berisi string "mkdir" sebagai argumen pertama dan `dir_name` sebagai argumen kedua. Kemudian, fungsi `execvp()` digunakan untuk menjalankan perintah "mkdir" dengan argumen yang telah dibuat. Fungsi `execvp()` akan mencari file "mkdir" pada daftar direktori `PATH` dan akan menjalankan program tersebut dengan argumen yang diberikan. Setelah perintah "mkdir" berhasil dijalankan, maka direktori baru dengan nama yang disimpan dalam variabel `dir_name` akan dibuat pada sistem operasi.

### 4. berikut adalah kode fungsi zip_folder()

```
void zip_folder(char* zip_name){
    pid_t pid;
    pid = fork();
    if (pid == 0)
    {
        char *argv[] = {"zip", "-r", zip_name, dir_name, NULL};
        execv("/bin/zip", argv);
        exit(0);
    } else if(pid>0){
        wait(NULL);
    }
}
```

Code tersebut adalah implementasi dari fungsi `zip_folder` yang menerima satu argumen berupa `char*` yang akan digunakan sebagai nama file zip yang akan dibuat. Fungsi ini bertujuan untuk melakukan zip pada folder dengan nama yang sudah ditentukan sebelumnya dalam variabel `dir_name`.

Pertama-tama, fungsi ini akan melakukan fork untuk membuat child process yang kemudian akan menjalankan perintah `zip` untuk melakukan kompresi terhadap folder dengan nama yang sudah ditentukan.

Di dalam child process, variabel `argv` digunakan untuk menyimpan argumen yang akan digunakan untuk menjalankan perintah `zip` melalui fungsi `execv`. Argumen tersebut terdiri dari string "zip", "-r", `zip_name`, dan `dir_name`. Argumen "-r" menandakan bahwa perintah `zip` akan melakukan rekursif untuk melakukan kompresi terhadap seluruh isi dari folder yang diberikan.

Setelah itu, fungsi `execv` akan dijalankan dengan argumen yang sudah disiapkan, dan child process akan keluar menggunakan `exit(0)`.

Jika child process telah selesai, maka parent process akan menunggu dengan menggunakan `wait(NULL)`, sehingga fungsi `zip_folder` akan selesai setelah proses zip selesai dilakukan.

### 5. berikut adalah kode fungsi hapus_folder()

```
void hapus_folder(){
    pid_t pid;
    pid=fork();

    if(pid==0){
        execlp("rm", "rm", "-r", dir_name, NULL);
        exit(0);
    }else if(pid>0){
        wait(NULL);
        printf("menghapus folder sukses!!\n");
    }
}
```

Fungsi `hapus_folder()` bertujuan untuk menghapus folder yang telah dibuat sebelumnya. Pertama, fungsi ini membuat sebuah child process menggunakan `fork()`. Jika `pid` bernilai 0, maka program masuk ke dalam child process dan menjalankan `execlp()` untuk menjalankan perintah "rm -r" pada direktori yang dihapus. Argumen "-r" berarti menghapus folder beserta seluruh isinya secara rekursif. Jika `pid` bernilai lebih besar dari 0, maka program masuk ke dalam parent process dan menunggu child process selesai menggunakan `wait()`. Setelah selesai, program akan mencetak pesan "menghapus folder sukses!!" sebagai tanda bahwa folder telah berhasil dihapus.

### 6. berikut adalah kode fungsi killer()

```
void killer(pid_t pid, char param){
    char cwd[100];
    //mendapatkan direktori saat ini
    getcwd(cwd, sizeof(cwd));
    int status;
    int test;
    //melakukan forking pada parent
    pid_t parent = fork();
    if (parent < 0) 
        exit(EXIT_FAILURE);

    if (parent == 0) {
        FILE *fp;
        char killer_file[200];
        char data[1000];
        char dest[200];
        char file_name[200];
        char mode[100];
        
        sprintf(file_name, "%s/killer.c", cwd);
        sprintf(dest, "%s/killer", cwd);

        sprintf(killer_file, "%s/killer.c", cwd);

        if (param == 'a') 
          strcpy(mode, " char *argv[] = {\"killall\", \"-s\", \"9\", \"lukisan\", NULL}; execv(\"/usr/bin/killall\", argv);");
        else if (param == 'b') 
          sprintf(mode, " char *argv[] = {\"kill\", \"%d\", NULL}; execv(\"/usr/bin/kill\", argv);", pid);
        
        sprintf(data,   "#include <stdlib.h>\n"
                        "#include <unistd.h>\n"
                        "#include <stdio.h>\n"
                        "#include <wait.h>\n"
                        "#include <string.h>\n"
                        "#include <sys/types.h>\n"

                        " int main() {"
                        " pid_t child_id;"
                        " int status;"

                        " child_id = fork();"
                        " if (child_id < 0) exit(EXIT_FAILURE);"

                        " if (child_id == 0) {"
                            " %s"
                        " } else {"
                            " while (wait(&status)>0);"
                            " char *argv[] = {\"rm\", \"%s/killer\", NULL};"
                            " execv(\"/bin/rm\", argv);"
                        " }"
                        " }", mode, cwd);

        fp = fopen(killer_file, "w");
        fputs(data, fp);
        fclose(fp);

        char *argv[] = {"gcc", file_name, "-o", dest, NULL};
        execv("/usr/bin/gcc", argv);
    }
    
    while(wait(&status) > 0);
    //melakukan forking pada remove_killer
    pid_t remove_killer = fork();
    if (remove_killer < 0) 
      exit(EXIT_FAILURE);

    if (remove_killer == 0) {
        char src[200];
        sprintf(src, "%s/killer.c", cwd);
        char *argv[] = {"rm", src, NULL};
        execv("/bin/rm", argv);
    }
    
    while(wait(&status) > 0);
}
```
Fungsi `killer()` ini memiliki tujuan untuk membuat file program yang dapat membunuh proses `lukisan` atau proses lain dengan menentukan parameter yang diberikan. File program ini akan dinamai `killer` dan disimpan di direktori tempat program yang dijalankan.

Pertama-tama, program akan mendapatkan direktori saat ini menggunakan fungsi `getcwd()` dan menyimpannya di variabel `cwd`. Lalu program melakukan forking pada parent, dan jika forking berhasil dilakukan maka akan memasuki child process. Di dalam child process, program akan membuat file `killer.c` yang berisi kode program sesuai dengan parameter yang diberikan. Jika parameter yang diberikan adalah 'a', maka program akan membuat kode untuk menjalankan perintah `killall -s 9 lukisan`. Sedangkan jika parameter yang diberikan adalah 'b', maka program akan membuat kode untuk menjalankan perintah `kill pid`, di mana `pid` adalah pid dari proses yang ingin dibunuh. Setelah membuat kode program, program akan meng-compile kode tersebut menggunakan gcc dengan mengeksekusi perintah `gcc killer.c -o killer`.

Setelah file `killer` berhasil dibuat, program akan melakukan wait pada parent process sampai child process yang pertama selesai. Lalu, program melakukan forking lagi untuk menghapus file `killer.c`. Di dalam child process baru ini, program akan mengeksekusi perintah `rm killer.c`.

Setelah file `killer.c` berhasil dihapus, maka fungsi `killer()` selesai. File killer yang telah dibuat dapat dijalankan dengan perintah `./killer` untuk membunuh proses `lukisan` atau proses lainnya sesuai dengan parameter yang diberikan.

### 7. berikut adalah kode fungsi main()

```
int main(int argc, char *argv[]){

    if(argc != 2 || argv[1][1] != 'a' && argv[1][1] != 'b'){
        printf("Argumen tidak tepat bro");
        return 0;
    }


    pid_t pid, sid;

    //membuat parent id
    pid = fork();

    if(pid < 0){
        exit(EXIT_FAILURE);
    }

    if(pid > 0){
        exit(EXIT_SUCCESS);
    }

    killer(getpid(), argv[1][1]);
    umask(0);

    sid = setsid();

    if(sid < 0){
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    killer(getpid(), argv[1][1]);
    while(1){

        //get time sekarang untuk penamaan directory
        time_t now = time(NULL);
        strftime(dir_name, 20, "%Y-%m-%d_%H:%M:%S", localtime(&now));


        pid_t id;
        id = fork();

        //membuat directory setiap 30 detik
        if (id == 0){ //child process
            make_dir(dir_name);
            // exit(0);
        }

        //menunggu pembuatan directory selama 1 detik
        sleep(1);

        pid_t pid2;
        pid2 = fork();

        //terjadi download image dan mengubah current directory menjadi zip setelah download 15 gambar
        if(pid2 == 0) { //parent process

            chdir(dir_name);

            for(int i = 1; i <= 15; i++){
                download_img();
                sleep(5);
            }
            
            chdir("..");
            char zip_name[100];
            sprintf(zip_name, "%s.zip", dir_name);

            zip_folder(zip_name);
            hapus_folder();
              
            exit(0);
        }
        sleep(29);
    }
    return EXIT_SUCCESS;

}
```

Code tersebut merupakan implementasi dari sebuah program yang akan melakukan download gambar setiap 5 detik dan akan membuat folder untuk menyimpan gambar-gambar tersebut. Selain itu, program juga akan melakukan zip pada folder tersebut setelah telah didownload 15 gambar dan akan menghapus folder tersebut.

Program tersebut memiliki beberapa fungsi, antara lain:

- `killer`: fungsi untuk membuat file `killer.c` dan mengkompilasi serta menjalankan program tersebut untuk menghapus program utama ketika dipanggil.
- `make_dir`: fungsi untuk membuat directory berdasarkan nama yang diberikan.
- `download_img`: fungsi untuk mendownload gambar dari link yang telah diberikan.
- `zip_folder`: fungsi untuk melakukan zip pada folder yang diberikan.
- `hapus_folder`: fungsi untuk menghapus folder yang diberikan.

Pada fungsi `main`, terdapat beberapa langkah yang dilakukan, antara lain:

- Mengecek apakah jumlah argumen yang diberikan tepat dan apakah argumen tersebut merupakan `a` atau `b`.
- Membuat process id parent dan melakukan exit.
- Memanggil fungsi `killer` untuk membuat file `killer.c` dan mengkompilasi serta menjalankan program tersebut untuk menghapus program utama ketika dipanggil.
- Membuat session id baru dengan `setsid`.
- Menutup file descriptor standar (STDIN, STDOUT, STDERR).
- Masuk ke dalam loop utama yang akan berjalan terus-menerus sampai program dihentikan.
- Membuat child process yang akan membuat directory setiap 30 detik menggunakan fungsi `make_dir`.
- Menunggu selama 1 detik setelah pembuatan directory.
- Membuat child process lagi yang akan mendownload gambar, dan akan melakukan zip pada folder tersebut setelah 15 gambar didownload. Setelah itu, folder akan dihapus menggunakan fungsi `hapus_folder`.
- Menunggu selama 29 detik sebelum melakukan loop lagi dari awal.

![WhatsApp Image 2023-04-08 at 21 45 23](https://user-images.githubusercontent.com/88433109/230728061-09f2f898-d417-4e97-9c0e-c9e849e4a795.jpg)

![WhatsApp Image 2023-04-08 at 21 45 43](https://user-images.githubusercontent.com/88433109/230728133-ea6dadac-80f1-4a68-ac2e-985f5eb03285.jpg)

# Nomor 3

Ten Hag adalah seorang pelatih Ajax di Liga Belanda. Suatu hari, Ten Hag mendapatkan tawaran untuk menjadi manajer Manchester United. Karena Ten Hag masih mempertimbangkan tawaran tersebut, ia ingin mengenal para pemain yang akan dilatih kedepannya. Dikarenakan Ten Hag hanya mendapatkan url atau link database mentah para pemain bola, maka ia perlu melakukan klasifikasi pemain Manchester United. Bantulah Ten Hag untuk mengenal para pemain Manchester United tersebut hanya dengan 1 Program C bernama “filter.c”

a. Pertama-tama, Program filter.c akan mengunduh file yang berisikan database para pemain bola. Kemudian dalam program yang sama diminta dapat melakukan extract “players.zip”. Lalu hapus file zip tersebut agar tidak memenuhi komputer Ten Hag.
b. Dikarenakan database yang diunduh masih data mentah. Maka bantulah Ten Hag untuk menghapus semua pemain yang bukan dari Manchester United yang ada di directory.  
c. Setelah mengetahui nama-nama pemain Manchester United, Ten Hag perlu untuk mengkategorikan pemain tersebut sesuai dengan posisi mereka dalam waktu bersamaan dengan 4 proses yang berbeda. Untuk kategori folder akan menjadi 4 yaitu Kiper, Bek, Gelandang, dan Penyerang.
d. Setelah mengkategorikan anggota tim Manchester United, Ten Hag memerlukan Kesebelasan Terbaik untuk menjadi senjata utama MU berdasarkan rating terbaik dengan wajib adanya kiper, bek, gelandang, dan penyerang. (Kiper pasti satu pemain). Untuk output nya akan menjadi Formasi_[jumlah bek]-[jumlah gelandang]-[jumlah penyerang].txt dan akan ditaruh di /home/[users]/

Jawaban :
```
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <dirent.h>
#include <string.h>
#define BUFFER_SIZE 1536


void donlod();
void unzip_delete();
void hapusNonMU();
void seleksiPosisi();
void buatTim(int, int, int);
void rapihinTeks(int, int, int);
// void seleksiPosisi();
// void buatTim();a


int main() {
    // tentukan formasi
    int formasi[3] = {4,3,3}; //{bek, gelandang, penyerang}

    pid_t pid1 = fork();

    if(pid1 == 0){
        donlod();
        unzip_delete();
        exit(0);
    } else if(pid1>0){
        wait(NULL);
        hapusNonMU();
        seleksiPosisi();
        buatTim(formasi[0], formasi[1], formasi[2]);
        rapihinTeks(formasi[0], formasi[1], formasi[2]);
        printf("NON MU SUDAH TERHAPUSSSSS\n");
    }
    
    return 0;
}


void donlod(){
    int stat;
    pid_t pid1 = fork();

    if(pid1 == 0){
        execl("/usr/bin/wget", "wget", "-O", "players.zip", "https://drive.google.com/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download", NULL);
    }
    while(wait(&stat) > 0) ;
    waitpid(pid1, &stat, 0);
}

void unzip_delete(){
    // int stat;
    pid_t pid1 = fork();

    if(pid1 == 0){
        execl("/usr/bin/unzip", "unzip", "players.zip", NULL);
        exit(0);
    } 
    else if (pid1 > 0){
        wait(NULL);
        execl("/bin/rm", "rm", "players.zip", NULL);
        // printf("File selection based on position has been completed\n");
    }
    

}

void hapusNonMU(){
    int pipefd[2];
    pid_t child_pid;
    char buffer[BUFFER_SIZE];

    // membuat pipe untuk menghubungkan parent dan child process
    if (pipe(pipefd) == -1) {
        perror("pipe");
        exit(EXIT_FAILURE);
    }

    // membuat child process
    child_pid = fork();

    if (child_pid == -1) {
        perror("fork");
        exit(EXIT_FAILURE);
    }

    if (child_pid == 0) {
        // child process
        // tutup file descriptor yang tidak digunakan oleh child process
        close(pipefd[0]);

        // redirect stdout ke pipe
        dup2(pipefd[1], STDOUT_FILENO);

        // jalankan command ls
        char *args[] = {"ls", "-1", "players", NULL};
        execv("/bin/ls", args);
    } else {
        // parent process
        // tutup file descriptor yang tidak digunakan oleh parent process
        close(pipefd[1]);

        // baca output dari child process menggunakan pipe
        ssize_t count = read(pipefd[0], buffer, BUFFER_SIZE);

        // menunggu child process selesai dijalankan
        waitpid(child_pid, NULL, 0);

        // menampilkan daftar file di console
        printf("Daftar file di dalam folder players:\n");
        printf("%.*s", (int)count, buffer);

        // iterasi pada daftar file dan hapus file yang tidak memiliki nama "ManCity"
        char* filename;
        filename = strtok(buffer, "\n");
        while (filename != NULL) {
            if (!strstr(filename, "ManUtd")) {
                char filepath[BUFFER_SIZE];
                snprintf(filepath, BUFFER_SIZE, "players/%s", filename);
                if (access(filepath, F_OK) != -1 ){
                    if (remove(filepath) == 0) {
                        printf("File '%s' telah dihapus.\n", filename);
                    } else {
                        printf("Gagal menghapus file '%s'.\n", filename);
                    }
                } else{
                     printf("Program tidak memiliki hak akses untuk menghapus file '%s'.\n", filename);
                }
                
            }
            filename = strtok(NULL, "\n");
        }
    }
}

void seleksiPosisi(){
    pid_t pid;

    pid = fork();
    if (pid == 0){
        char *args[] = {"mkdir", "Kiper", NULL};
        execvp(args[0], args);
        printf("Failed to execute mkdir kiper command\n");
    }
    else if (pid > 0){
        wait(NULL);

        pid = fork();
        if (pid == 0){
            char *args2[] = {"find", "players", "-type", "f", "-name", "*Kiper*", "-exec", "mv", "{}", "Kiper/", ";", NULL};
            execvp(args2[0], args2);
            printf("Failed to execute find Kiper command\n");
        }
        else if (pid > 0){
            wait(NULL);

            pid = fork();
            if (pid == 0){
                char *args[] = {"mkdir", "Bek", NULL};
                execvp(args[0], args);
                printf("Failed to execute mkdir Bek command\n");
            }
            else if (pid > 0){
                wait(NULL);

                pid = fork();
                if (pid == 0){
                    char *args3[] = {"find", "players", "-type", "f", "-name", "*Bek*", "-exec", "mv", "{}", "Bek/", ";", NULL};
                    execvp(args3[0], args3);
                    printf("Failed to execute find bek command\n");
                }
                else if (pid > 0){
                    wait(NULL);

                    pid = fork();
                    if (pid == 0){
                        char *args[] = {"mkdir", "Gelandang", NULL};
                        execvp(args[0], args);
                        printf("Failed to execute mkdir Gelandang command\n");
                    }
                    else if (pid > 0){
                        wait(NULL);

                        pid = fork();
                        if (pid == 0){
                            char *args4[] = {"find", "players", "-type", "f", "-name", "*Gelandang*", "-exec", "mv", "{}", "Gelandang/", ";", NULL};
                            execvp(args4[0], args4);
                            printf("Failed to execute find Gelandang command\n");
                        }
                        else if (pid > 0){
                            wait(NULL);

                            pid = fork();
                            if (pid == 0){
                                char *args[] = {"mkdir", "Penyerang", NULL};
                                execvp(args[0], args);
                                printf("Failed to execute mkdir Penyerang command\n");
                            }
                            else if (pid > 0){
                                wait(NULL);

                                pid = fork();
                                if (pid == 0){
                                    char *args5[] = {"find", "players", "-type", "f", "-name", "*Penyerang*", "-exec", "mv", "{}", "Penyerang/", ";", NULL};
                                    execvp(args5[0], args5);
                                    printf("Failed to execute find Penyerang command\n");
                                }
                                else if (pid > 0){
                                    wait(NULL);
                                    printf("File selection based on position has been completed\n");
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

void buatTim(int jum_bek, int jum_gelandang, int jum_penyerang){

    pid_t pid;
    // char fileName[30];
    // sprintf(fileName, "Formasi_%d_%d_%d.txt", jum_bek, jum_gelandang, jum_penyerang);

    pid = fork();

    if(pid == 0){
        //kiper
        char command[120];

        // jalankan perintah untuk mencari 3 nilai tertinggi dalam folder "bek"
        sprintf(command, "ls Kiper | sort -t_ -k4 -nr | head -n %d | awk -F_ '{print $1\"_\"$2\"_\"$3\"_\"$4}' | xargs >> Formasi_%d_%d_%d.txt", 1, jum_bek, jum_gelandang, jum_penyerang);
        execlp("sh", "sh", "-c", command, NULL);

        // jika exec berhasil, kode di bawah ini tidak akan dieksekusi
        perror("exec failed");
        exit(EXIT_FAILURE);
    }else if(pid > 0){
        wait(NULL);

        pid = fork();
        if(pid == 0){
            //bek

            char command[120];

            // jalankan perintah untuk mencari 3 nilai tertinggi dalam folder "bek"
            sprintf(command, "ls Bek | sort -t_ -k4 -nr | head -n %d | awk -F_ '{print $1\"_\"$2\"_\"$3\"_\"$4}' | xargs >> Formasi_%d_%d_%d.txt", jum_bek, jum_bek, jum_gelandang, jum_penyerang);
            execlp("sh", "sh", "-c", command, NULL);

            // jika exec berhasil, kode di bawah ini tidak akan dieksekusi
            perror("exec failed");
            exit(EXIT_FAILURE);
        }else if(pid>0){
            wait(NULL);

            pid = fork();
            if(pid == 0){
                //gelandang
                char command[120];

                // jalankan perintah untuk mencari 3 nilai tertinggi dalam folder "bek"
                sprintf(command, "ls Gelandang | sort -t_ -k4 -nr | head -n %d | awk -F_ '{print $1\"_\"$2\"_\"$3\"_\"$4}' | xargs >> Formasi_%d_%d_%d.txt", jum_gelandang, jum_bek, jum_gelandang, jum_penyerang);
                execlp("sh", "sh", "-c", command, NULL);

                // jika exec berhasil, kode di bawah ini tidak akan dieksekusi
                perror("exec failed");
                exit(EXIT_FAILURE);
            }else if(pid>0){
                wait(NULL);

                pid = fork();
                if(pid==0){
                    //penyerang
                    char command[120];

                    // jalankan perintah untuk mencari 3 nilai tertinggi dalam folder "bek"
                    sprintf(command, "ls Penyerang | sort -t_ -k4 -nr | head -n %d | awk -F_ '{print $1\"_\"$2\"_\"$3\"_\"$4}' | xargs >> Formasi_%d_%d_%d.txt", jum_penyerang, jum_bek, jum_gelandang, jum_penyerang);
                    execlp("sh", "sh", "-c", command, NULL);

                    // jika exec berhasil, kode di bawah ini tidak akan dieksekusi
                    perror("exec failed");
                    exit(EXIT_FAILURE);
                }else if(pid>0){
                    // wait for child process to finish
                    wait(NULL);
                    printf("suksesssss\n");

                }
            }
        }
    }
}


void rapihinTeks(int jum_bek, int jum_gelandang, int jum_penyerang){
    char command[120];
    pid_t pid;
    pid=fork();
    if(pid==0){
        sprintf(command, "tr ' ' '\n' < Formasi_%d_%d_%d.txt >tmp.txt && mv tmp.txt Formasi_%d_%d_%d.txt",jum_bek, jum_gelandang, jum_penyerang, jum_bek, jum_gelandang, jum_penyerang );
        execlp("sh", "sh", "-c", command, NULL);
    }else if(pid>0){
        wait(NULL);
    }
}

```

1. Berikut merupakan header file yang diperlukan dalam program

```
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <dirent.h>
#include <string.h>
#define BUFFER_SIZE 1536
```
- `#include <stdio.h>` untuk fungsi input output standar seperti printf() dan scanf().
- `#include <stdlib.h>` untuk fungsi umum seperti atoi() dan exit().
- `#include <unistd.h>` untuk fungsi sistem seperti fork() dan exec().
- `#include <sys/types.h>` untuk tipe data tertentu seperti pid_t.
- `#include <sys/wait.h>` untuk fungsi pengelolaan proses child seperti wait().
- `#include <dirent.h>`  untuk membaca direktori pada sistem operasi Unix/Linux.
- `#include <string.h>` untuk fungsi manipulasi string seperti strcmp().
- `#define BUFFER_SIZE 1536` untuk mendefinisikan sebuah konstanta bernama 'BUFFER_SIZE' yang memiliki nilai 1536.

2. Berikut kode fungsi untuk download File
```
void donlod(){
    int stat;
    pid_t pid1 = fork();

    if(pid1 == 0){
        execl("/usr/bin/wget", "wget", "-O", "players.zip", "https://drive.google.com/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download", NULL);
    }
    while(wait(&stat) > 0) ;
    waitpid(pid1, &stat, 0);
}

```

- Fungsi ini menggunakan fungsi fork() untuk membuat proses baru. Jika pid1 sama dengan 0, ini berarti proses baru telah dibuat dan kode yang terdapat di dalam if statement akan dijalankan oleh proses baru tersebut.
- Dalam if statement, fungsi execl() digunakan untuk menjalankan perintah "wget" di direktori "/usr/bin/wget" dengan opsi "-O" untuk menentukan nama file hasil unduhan (yaitu "players.zip") dan URL yang akan diunduh. Argumen terakhir NULL menandakan akhir dari daftar argumen.
- Setelah perintah unduhan selesai dieksekusi, parent process (proses utama) menunggu child process (proses anak) selesai dieksekusi dengan menggunakan perintah wait(). Setelah itu, parent process menunggu kembali child process yang telah dibuat sebelumnya (pid1) selesai dieksekusi dengan menggunakan perintah waitpid().

3. Berikut adalah kode fungsi untuk unzip dan delete file
```
void unzip_delete(){
    // int stat;
    pid_t pid1 = fork();

    if(pid1 == 0){
        execl("/usr/bin/unzip", "unzip", "players.zip", NULL);
        exit(0);
    } 
    else if (pid1 > 0){
        wait(NULL);
        execl("/bin/rm", "rm", "players.zip", NULL);
        // printf("File selection based on position has been completed\n");
    }
    

}

```

- Pertama-tama, program akan membuat proses baru dengan menggunakan sistem panggilan fork(), dan memeriksa apakah proses yang dibuat berhasil atau tidak dengan memeriksa nilai yang dikembalikan oleh fork()
-  Jika nilai yang dikembalikan adalah 0, artinya proses yang dijalankan saat ini adalah proses child, dan program akan menjalankan sistem panggilan execl() untuk mengekstraksi file ZIP yang bernama "players.zip" dengan perintah unzip players.zip. Setelah itu, program akan keluar dari proses child menggunakan sistem panggilan exit(0).
- Sementara itu, jika nilai yang dikembalikan oleh fork() adalah lebih besar dari 0, artinya proses yang dijalankan saat ini adalah proses parent. Program akan menunggu proses child selesai dieksekusi dengan menggunakan sistem panggilan wait(NULL). 
- Setelah itu, program akan menjalankan sistem panggilan execl() lagi untuk menghapus file ZIP "players.zip" dengan perintah rm players.zip menggunakan sistem panggilan /bin/rm. Setelah selesai, program akan selesai dijalankan.

4. Berikut kode program untuk menghapus pemain yang bukan MU
```
void hapusNonMU(){
    int pipefd[2];
    pid_t child_pid;
    char buffer[BUFFER_SIZE];

    // membuat pipe untuk menghubungkan parent dan child process
    if (pipe(pipefd) == -1) {
        perror("pipe");
        exit(EXIT_FAILURE);
    }

    // membuat child process
    child_pid = fork();

    if (child_pid == -1) {
        perror("fork");
        exit(EXIT_FAILURE);
    }

    if (child_pid == 0) {
        // child process
        // tutup file descriptor yang tidak digunakan oleh child process
        close(pipefd[0]);

        // redirect stdout ke pipe
        dup2(pipefd[1], STDOUT_FILENO);

        // jalankan command ls
        char *args[] = {"ls", "-1", "players", NULL};
        execv("/bin/ls", args);
    } else {
        // parent process
        // tutup file descriptor yang tidak digunakan oleh parent process
        close(pipefd[1]);

        // baca output dari child process menggunakan pipe
        ssize_t count = read(pipefd[0], buffer, BUFFER_SIZE);

        // menunggu child process selesai dijalankan
        waitpid(child_pid, NULL, 0);

        // menampilkan daftar file di console
        printf("Daftar file di dalam folder players:\n");
        printf("%.*s", (int)count, buffer);

        // iterasi pada daftar file dan hapus file yang tidak memiliki nama "ManCity"
        char* filename;
        filename = strtok(buffer, "\n");
        while (filename != NULL) {
            if (!strstr(filename, "ManUtd")) {
                char filepath[BUFFER_SIZE];
                snprintf(filepath, BUFFER_SIZE, "players/%s", filename);
                if (access(filepath, F_OK) != -1 ){
                    if (remove(filepath) == 0) {
                        printf("File '%s' telah dihapus.\n", filename);
                    } else {
                        printf("Gagal menghapus file '%s'.\n", filename);
                    }
                } else{
                     printf("Program tidak memiliki hak akses untuk menghapus file '%s'.\n", filename);
                }
                
            }
            filename = strtok(NULL, "\n");
        }
    }
}

```

- Pertama, fungsi ini akan membuat sebuah pipe untuk menghubungkan antara parent process dan child process. Lalu, membuat sebuah child process dengan melakukan fork. Child process akan mengeksekusi command ls -1 players menggunakan fungsi execv, dan mengirimkan output dari command tersebut melalui pipe. Parent process akan membaca output dari child process menggunakan fungsi read.
- Setelah itu, parent process akan menunggu child process selesai dijalankan menggunakan fungsi waitpid. Kemudian, parent process akan menampilkan daftar file di dalam folder players menggunakan printf. Selanjutnya, parent process akan melakukan iterasi pada daftar file dan menghapus file yang tidak memiliki nama "ManUtd". Untuk menghapus file, parent process menggunakan fungsi remove. Jika file tidak dapat dihapus, maka akan ditampilkan pesan error. Jika file tidak dapat diakses, maka akan ditampilkan pesan bahwa program tidak memiliki hak akses untuk menghapus file tersebut.
- Kemudian, fungsi ini akan selesai dieksekusi.

5. Berikut kode program untuk seleksi posisi
```
void seleksiPosisi(){
    pid_t pid;

    pid = fork();
    if (pid == 0){
        char *args[] = {"mkdir", "Kiper", NULL};
        execvp(args[0], args);
        printf("Failed to execute mkdir kiper command\n");
    }
    else if (pid > 0){
        wait(NULL);

        pid = fork();
        if (pid == 0){
            char *args2[] = {"find", "players", "-type", "f", "-name", "*Kiper*", "-exec", "mv", "{}", "Kiper/", ";", NULL};
            execvp(args2[0], args2);
            printf("Failed to execute find Kiper command\n");
        }
        else if (pid > 0){
            wait(NULL);

            pid = fork();
            if (pid == 0){
                char *args[] = {"mkdir", "Bek", NULL};
                execvp(args[0], args);
                printf("Failed to execute mkdir Bek command\n");
            }
            else if (pid > 0){
                wait(NULL);

                pid = fork();
                if (pid == 0){
                    char *args3[] = {"find", "players", "-type", "f", "-name", "*Bek*", "-exec", "mv", "{}", "Bek/", ";", NULL};
                    execvp(args3[0], args3);
                    printf("Failed to execute find bek command\n");
                }
                else if (pid > 0){
                    wait(NULL);

                    pid = fork();
                    if (pid == 0){
                        char *args[] = {"mkdir", "Gelandang", NULL};
                        execvp(args[0], args);
                        printf("Failed to execute mkdir Gelandang command\n");
                    }
                    else if (pid > 0){
                        wait(NULL);

                        pid = fork();
                        if (pid == 0){
                            char *args4[] = {"find", "players", "-type", "f", "-name", "*Gelandang*", "-exec", "mv", "{}", "Gelandang/", ";", NULL};
                            execvp(args4[0], args4);
                            printf("Failed to execute find Gelandang command\n");
                        }
                        else if (pid > 0){
                            wait(NULL);

                            pid = fork();
                            if (pid == 0){
                                char *args[] = {"mkdir", "Penyerang", NULL};
                                execvp(args[0], args);
                                printf("Failed to execute mkdir Penyerang command\n");
                            }
                            else if (pid > 0){
                                wait(NULL);

                                pid = fork();
                                if (pid == 0){
                                    char *args5[] = {"find", "players", "-type", "f", "-name", "*Penyerang*", "-exec", "mv", "{}", "Penyerang/", ";", NULL};
                                    execvp(args5[0], args5);
                                    printf("Failed to execute find Penyerang command\n");
                                }
                                else if (pid > 0){
                                    wait(NULL);
                                    printf("File selection based on position has been completed\n");
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

```
- Pertama-tama, fungsi ini membuat sebuah direktori "Kiper" menggunakan perintah "mkdir" pada child process pertama yang dihasilkan oleh fork(). Setelah itu, child process tersebut selesai dijalankan dan parent process menunggu child process tersebut selesai dengan menggunakan perintah wait(NULL).
- Kemudian, parent process membuat child process kedua untuk menjalankan perintah "find" yang akan mencari file-file dalam direktori "players" dengan nama yang mengandung string "Kiper" dan memindahkannya ke direktori "Kiper" menggunakan perintah "mv". Setelah itu, parent process menunggu child process kedua tersebut selesai dengan menggunakan perintah wait(NULL).
- Langkah-langkah yang sama dilakukan untuk memilah file berdasarkan posisi pemain lainnya, yaitu "Bek", "Gelandang", dan "Penyerang". Setelah semua child process selesai dijalankan, parent process akan menampilkan pesan "File selection based on position has been completed".

6. Berikut kode program untuk membuat tim
```
void buatTim(int jum_bek, int jum_gelandang, int jum_penyerang){

    pid_t pid;
    // char fileName[30];
    // sprintf(fileName, "Formasi_%d_%d_%d.txt", jum_bek, jum_gelandang, jum_penyerang);

    pid = fork();

    if(pid == 0){
        //kiper
        char command[120];

        // jalankan perintah untuk mencari 3 nilai tertinggi dalam folder "bek"
        sprintf(command, "ls Kiper | sort -t_ -k4 -nr | head -n %d | awk -F_ '{print $1\"_\"$2\"_\"$3\"_\"$4}' | xargs >> Formasi_%d_%d_%d.txt", 1, jum_bek, jum_gelandang, jum_penyerang);
        execlp("sh", "sh", "-c", command, NULL);

        // jika exec berhasil, kode di bawah ini tidak akan dieksekusi
        perror("exec failed");
        exit(EXIT_FAILURE);
    }else if(pid > 0){
        wait(NULL);

        pid = fork();
        if(pid == 0){
            //bek

            char command[120];

            // jalankan perintah untuk mencari 3 nilai tertinggi dalam folder "bek"
            sprintf(command, "ls Bek | sort -t_ -k4 -nr | head -n %d | awk -F_ '{print $1\"_\"$2\"_\"$3\"_\"$4}' | xargs >> Formasi_%d_%d_%d.txt", jum_bek, jum_bek, jum_gelandang, jum_penyerang);
            execlp("sh", "sh", "-c", command, NULL);

            // jika exec berhasil, kode di bawah ini tidak akan dieksekusi
            perror("exec failed");
            exit(EXIT_FAILURE);
        }else if(pid>0){
            wait(NULL);

            pid = fork();
            if(pid == 0){
                //gelandang
                char command[120];

                // jalankan perintah untuk mencari 3 nilai tertinggi dalam folder "bek"
                sprintf(command, "ls Gelandang | sort -t_ -k4 -nr | head -n %d | awk -F_ '{print $1\"_\"$2\"_\"$3\"_\"$4}' | xargs >> Formasi_%d_%d_%d.txt", jum_gelandang, jum_bek, jum_gelandang, jum_penyerang);
                execlp("sh", "sh", "-c", command, NULL);

                // jika exec berhasil, kode di bawah ini tidak akan dieksekusi
                perror("exec failed");
                exit(EXIT_FAILURE);
            }else if(pid>0){
                wait(NULL);

                pid = fork();
                if(pid==0){
                    //penyerang
                    char command[120];

                    // jalankan perintah untuk mencari 3 nilai tertinggi dalam folder "bek"
                    sprintf(command, "ls Penyerang | sort -t_ -k4 -nr | head -n %d | awk -F_ '{print $1\"_\"$2\"_\"$3\"_\"$4}' | xargs >> Formasi_%d_%d_%d.txt", jum_penyerang, jum_bek, jum_gelandang, jum_penyerang);
                    execlp("sh", "sh", "-c", command, NULL);

                    // jika exec berhasil, kode di bawah ini tidak akan dieksekusi
                    perror("exec failed");
                    exit(EXIT_FAILURE);
                }else if(pid>0){
                    // wait for child process to finish
                    wait(NULL);
                    printf("suksesssss\n");

                }
            }
        }
    }
}

```

- Pada awalnya, fungsi ini melakukan fork pertama dan memeriksa apakah proses yang sedang berjalan adalah proses induk atau anak dengan mengecek nilai pid yang dikembalikan oleh fungsi fork. Jika nilai pid adalah 0, ini berarti proses ini adalah proses anak, dan proses ini kemudian menjalankan perintah shell untuk mencari 1 file dengan nilai tertinggi dalam folder Kiper
-  File-file ini kemudian ditulis ke dalam file bernama Formasi_jum_bek_jum_gelandang_jum_penyerang.txt. Jika perintah shell berhasil dijalankan, maka kode di dalam if statement akan dijalankan. Jika tidak, maka program akan mencetak pesan kesalahan dan keluar.
- Setelah proses anak pertama selesai, proses induk menunggu proses anak pertama selesai menggunakan fungsi wait(). Setelah proses anak pertama selesai, proses induk melakukan fork kedua untuk menjalankan proses anak kedua untuk memproses folder Bek.
- Jika proses yang sedang berjalan adalah proses anak, proses ini akan menjalankan perintah shell untuk mencari tiga file dengan nilai tertinggi dalam folder Bek. File-file ini kemudian ditulis ke dalam file Formasi_jum_bek_jum_gelandang_jum_penyerang.txt. Jika perintah shell berhasil dijalankan, maka kode di dalam if statement akan dijalankan. Jika tidak, maka program akan mencetak pesan kesalahan dan keluar.
- Setelah proses anak pertama selesai, proses induk menunggu proses anak pertama selesai menggunakan fungsi wait(). Setelah proses anak pertama selesai, proses induk melakukan fork kedua untuk menjalankan proses anak kedua untuk memproses folder Bek. Jika proses yang sedang berjalan adalah proses anak, proses ini akan menjalankan perintah shell untuk mencari tiga file dengan nilai tertinggi dalam folder Bek. File-file ini kemudian ditulis ke dalam file Formasi_jum_bek_jum_gelandang_jum_penyerang.txt. Jika perintah shell berhasil dijalankan, maka kode di dalam if statement akan dijalankan. Jika tidak, maka program akan mencetak pesan kesalahan dan keluar.
- Setelah proses anak kedua selesai, proses induk menunggu proses anak kedua selesai menggunakan fungsi wait(). Kemudian, proses induk melakukan fork ketiga untuk menjalankan proses anak ketiga untuk memproses folder Gelandang. Jika proses yang sedang berjalan adalah proses anak, proses ini akan menjalankan perintah shell untuk mencari tiga file dengan nilai tertinggi dalam folder Gelandang. File-file ini kemudian ditulis ke dalam file Formasi_jum_bek_jum_gelandang_jum_penyerang.txt. Jika perintah shell berhasil dijalankan, maka kode di dalam if statement akan dijalankan. Jika tidak, maka program akan mencetak pesan kesalahan dan keluar.
- Setelah proses anak ketiga selesai, proses induk menunggu proses anak ketiga selesai menggunakan fungsi wait(). Kemudian, proses induk melakukan fork keempat untuk menjalankan proses anak keempat untuk memproses folder Penyerang. Jika proses yang sedang berjalan adalah proses anak, proses ini akan menjalankan perintah shell untuk mencari tiga file dengan nilai tertinggi dalam folder Penyerang. File-file ini kemudian ditulis ke dalam file Formasi_jum_bek_jum_gelandang_jum_penyerang.txt. Jika perintah shell berhasil dijalankan, maka kode dibawahnya tidak akan dieksekusi. Jika gagal, maka pesan kesalahan akan dicetak menggunakan fungsi perror() dan program akan keluar dengan status EXIT_FAILURE.
- Jika proses yang sedang berjalan adalah proses induk, setelah menunggu proses anak keempat selesai menggunakan fungsi wait(), program mencetak pesan "suksesssss" ke layar sebagai tanda bahwa semua proses telah selesai dijalankan dan file Formasi_jum_bek_jum_gelandang_jum_penyerang.txt sudah terisi dengan benar.

7. Berikut adalah kode program untuk merapikan isi file
```
void rapihinTeks(int jum_bek, int jum_gelandang, int jum_penyerang){
    char command[120];
    pid_t pid;
    pid=fork();
    if(pid==0){
        sprintf(command, "tr ' ' '\n' < Formasi_%d_%d_%d.txt >tmp.txt && mv tmp.txt Formasi_%d_%d_%d.txt",jum_bek, jum_gelandang, jum_penyerang, jum_bek, jum_gelandang, jum_penyerang );
        execlp("sh", "sh", "-c", command, NULL);
    }else if(pid>0){
        wait(NULL);
    }
}

```
- Pada awal fungsi, sebuah variabel command bertipe char dengan panjang 120 karakter dideklarasikan. Selanjutnya, sebuah proses anak dibuat menggunakan fungsi fork(). Jika proses yang sedang berjalan adalah proses anak, maka variabel command akan diisi dengan perintah shell untuk merapikan teks di dalam file Formasi_jum_bek_jum_gelandang_jum_penyerang.txt.
- Perintah shell ini menggunakan command-line utility tr untuk mengganti spasi dengan karakter newline dan menyimpan outputnya di file sementara tmp.txt. Setelah itu, file sementara tersebut diubah namanya menjadi Formasi_jum_bek_jum_gelandang_jum_penyerang.txt menggunakan command-line utility mv.
- Jika proses yang sedang berjalan adalah proses induk, proses ini akan menunggu proses anak selesai menggunakan fungsi wait(). Setelah proses anak selesai, fungsi rapihinTeks() akan berakhir.

 ![WhatsApp Image 2023-04-08 at 21 21 49](https://user-images.githubusercontent.com/88433109/230727277-15d27fa5-4712-40c3-96d5-255238ae73e8.jpg)

 ![WhatsApp Image 2023-04-08 at 21 47 20](https://user-images.githubusercontent.com/88433109/230727745-eec39d8d-4307-4485-aeaa-8776368e6b20.jpg)

 ![WhatsApp Image 2023-04-08 at 21 48 07](https://user-images.githubusercontent.com/88433109/230727815-a373cc5b-16c8-424b-9da9-ee5ddf289097.jpg)


# Nomor 4

Banabil adalah seorang mahasiswa yang rajin kuliah dan suka belajar. Namun naasnya Banabil salah mencari teman, dia diajak ke toko mainan oleh teman-temannya dan teracuni untuk membeli banyak sekali mainan dan kebingungan memilih mainan mana yang harus dibeli. Hal tersebut menyebabkan Banabil kehilangan fokus dalam pengerjaan tugas-tugas yang diberikan oleh dosen nya. Untuk mengembalikan fokusnya, Banabil harus melatih diri sendiri dalam membuat program untuk menjalankan script bash yang menyerupai crontab dan menggunakan bahasa C karena baru dipelajari olehnya.


Untuk menambah tantangan agar membuatnya semakin terfokus, Banabil membuat beberapa ketentuan custom yang harus dia ikuti sendiri. Ketentuan tersebut berupa:
- Banabil tidak ingin menggunakan fungsi system(), karena terlalu mudah.
- Dalam pelatihan fokus time managementnya, Banabil harus bisa membuat program yang dapat menerima argumen berupa Jam (0-23), Menit (0-59), Detik (0-59), Tanda asterisk [ * ] (value bebas), serta path file .sh.
- Dalam pelatihan fokus untuk ketepatan pilihannya, Banabil ingin programnya dapat mengeluarkan pesan “error” apabila argumen yang diterima program tidak sesuai. Pesan error dapat dibentuk sesuka hati oleh pembuat program. terserah bagaimana, yang penting tulisan error.
- Terakhir, dalam pelatihan kesempurnaan fokusnya, Banabil ingin program ini berjalan dalam background dan hanya menerima satu config cron.
- Bonus poin apabila CPU state minimum.

Contoh untuk run: /program * 44 5 /home/Banabil/programcron.sh

Jawaban :
```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <stdbool.h>


int main(int argc, char * argv[]) {
  if (argc != 5) {
    printf("Error: Wrong number of arguments");
    return 1;

  }

  if (strcmp(argv[1], "*") != 0 && (atoi(argv[1]) < 0 || atoi(argv[1]) > 23)) {
    printf("Error: Wrong hour argument");
    return 1;
  }

  if (strcmp(argv[2], "*") != 0 && (atoi(argv[2]) < 0 || atoi(argv[2]) > 59)) {
    printf("Error: Wrong minute argument");
    return 1;
  }

  if (strcmp(argv[3], "*") != 0 && (atoi(argv[3]) < 0 || atoi(argv[3]) > 59)) {
    printf("Error: Wrong second argument");
    return 1;
  }

  if (access(argv[4], F_OK) == -1) {
    printf("Error: File does not exist");
    return 1;
  }

  time_t t = time(NULL);
  struct tm tm = * localtime( & t);
  int curr_h = tm.tm_hour;
  int curr_m = tm.tm_min;
  int curr_s = tm.tm_sec;

  int hour = atoi(argv[1]);
  int minute = atoi(argv[2]);
  int second = atoi(argv[3]);

  if (strcmp(argv[1], "*") == 0) {
    hour = -1;
  }
  if (strcmp(argv[2], "*") == 0) {
    minute = -1;
  }
  if (strcmp(argv[3], "*") == 0) {
    second = -1;
  }

  int firstSleep = 0;

  if (hour == -1) {
    if (minute == -1) {
      if (second >= 0) {
        if (second < curr_s) {
          firstSleep = (second + 60) - curr_s;
        } else {
          firstSleep = second - curr_s;
        }
      } else {
        firstSleep = 1;
      }
    } else {
      firstSleep = (((minute * 60) + second) - ((curr_m * 60) + curr_s));
      if (minute < curr_m || (minute == curr_m && second < curr_s)) {
        firstSleep += 3600;
      }
    }
  } else {
    firstSleep = (((hour * 3600) + (minute * 60) + second) - ((curr_h * 3600) + (curr_m * 60) + curr_s));
    if (hour < curr_h || (hour == curr_h && (minute < curr_m || (minute == curr_m && second < curr_s)))) {
      firstSleep += 86400;
    }
  }

  if (hour == -1 && minute == -1 && second == -1) {
    firstSleep = 1;
  }

  bool first = true;

  pid_t pid = fork();
  if (pid < 0) {
    exit(EXIT_FAILURE);
  }

  if (pid > 0) {
    exit(EXIT_SUCCESS);
  }

  if (setsid() < 0) {
    exit(EXIT_FAILURE);
  }

  close(STDIN_FILENO);
  close(STDOUT_FILENO);
  close(STDERR_FILENO);

  while (1) {

    int sleepTime = 0;
    if (hour == -1 && minute == -1 && second == -1) {
      sleepTime = 1;
    }
    if (hour != -1) {
      sleepTime += 86400;
    } else if (minute != -1) {
      sleepTime += 3600;
    } else if (second != -1) {
      sleepTime += 60;
    }

    if (first) {
      sleep(firstSleep);
      first = false;

      if (hour == -1) {
        if (minute == -1) {
          if (second == -1) {
            sleepTime = 1;
          }
        } else {
          if (second == -1) {
            sleepTime -= curr_s;
          }
        }

      } else {
        if (minute == -1 && second == -1) {
          sleepTime -= ((curr_m * 60) + curr_s);
        } else if (minute == -1 && second >= 0) {
          sleepTime -= (curr_m * 60);
        } else if (minute >= 0 && second == -1) {
          sleepTime -= curr_s;
        }

      }

    }

    pid_t cid = fork();

    if (cid == -1) {
      printf("Error : failed to fork proccess\n");
    } else if (cid == 0) {
      char * args[] = {
        "bash",
        argv[4],
        NULL
      };
      execv("/bin/bash", args);
    } else {
      wait(NULL);
    }

    sleep(sleepTime);

  }

  return 0;

}

```

Penjelasan :

1. Berikut merupakan header file yang diperlukan dalam program

```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <stdbool.h>
```
- `#include <stdio.h>` untuk fungsi input output standar seperti printf() dan scanf().
- `#include <stdlib.h>` untuk fungsi umum seperti atoi() dan exit().
- `#include <string.h>` untuk fungsi manipulasi string seperti strcmp().
- `#include <unistd.h>` untuk fungsi sistem seperti fork() dan exec().
- `#include <sys/types.h>` untuk tipe data tertentu seperti pid_t.
- `#include <sys/wait.h>` untuk fungsi pengelolaan proses child seperti wait().
- `#include <time.h>` untuk manipulasi wakttu seperti time().
- `#include <stdbool.h>` untuk tipe data boleean.


2. Berikut merupakan pengecekan argumen
```
if (argc != 5) {
    printf("Error: Wrong number of arguments");
    return 1;

  }

  if (strcmp(argv[1], "*") != 0 && (atoi(argv[1]) < 0 || atoi(argv[1]) > 23)) {
    printf("Error: Wrong hour argument");
    return 1;
  }

  if (strcmp(argv[2], "*") != 0 && (atoi(argv[2]) < 0 || atoi(argv[2]) > 59)) {
    printf("Error: Wrong minute argument");
    return 1;
  }

  if (strcmp(argv[3], "*") != 0 && (atoi(argv[3]) < 0 || atoi(argv[3]) > 59)) {
    printf("Error: Wrong second argument");
    return 1;
  }

  if (access(argv[4], F_OK) == -1) {
    printf("Error: File does not exist");
    return 1;
  }

```

- `if (argc != 5) dst...` mengecek apakah jumlah argumen yang diterima sama dengan 5, jika tidak maka mencetak pesan error dan keluar program dengan kode error 1.
- `if (strcmp(argv[1], "*") != 0 && (atoi(argv[1]) < 0 || atoi(argv[1]) > 23)) dst...` 
- `if (strcmp(argv[2], "*") != 0 && (atoi(argv[2]) < 0 || atoi(argv[2]) > 59)) dst...` 
- `if (strcmp(argv[3], "*") != 0 && (atoi(argv[3]) < 0 || atoi(argv[3]) > 59)) dst...`

mengecek apakah argumen jam, menit, dan detik berada pada rentang nilai yang benar atau tidak. Jika salah satu dari ketiganya tidak pada rentang yang benar atau tidak sesuai dengan karakter '*', maka mencetak pesan error sesuai dengan jenis argumen dan keluar program dengan kode error 1. validasi menggunakan fungsi strcmp() untuk membandingkan 2 buah string dan menggunakan fungsi atoi() untuk mengonversi string menjadi integer.
- `if (access(argv[4], F_OK) == -1)` mengecek apakah file yang berada pada argumen ada atau tidak dengan menggunakan fungsi access(). Jika file tidak ditemukan, maka mencetak pesan error dan keluar program dengan kode error 1.

2. Berikut kode untuk inisialisasi waktu dan mengubah format waktu 
```
time_t t = time(NULL);
  struct tm tm = * localtime( & t);
  int curr_h = tm.tm_hour;
  int curr_m = tm.tm_min;
  int curr_s = tm.tm_sec;

  int hour = atoi(argv[1]);
  int minute = atoi(argv[2]);
  int second = atoi(argv[3]);

  if (strcmp(argv[1], "*") == 0) {
    hour = -1;
  }
  if (strcmp(argv[2], "*") == 0) {
    minute = -1;
  }
  if (strcmp(argv[3], "*") == 0) {
    second = -1;
  }


```
- `time_t t = time(NULL)` deklarasi variabel bertipe time_t bernama t untuk menyimpan nilai waktu dengan fungsi time(). NULL diisi sebagai parameter fungsi time() untuk mengabaikan informasi mengenai zona waktu tertentu dan hanya memperoleh informasi waktu saat ini. Dapat dikatakan bahwa kita memanggil fungsi time() tanpa parameter.
- `struct tm tm = * localtime( & t)` mengubah waktu yang diperoleh pada fungsi time() ke dalam format struct tm menggunakan fungsi localtime.
- `int curr_h = tm.tm_hour`
- `int curr_m = tm.tm_min`
- `int curr_s = tm.tm_sec`

memisahkan waktu menjadi bagian jam, menit, dan detik dan menyimpan pada variabel curr_h, curr_m, curr_s.
- `if (strcmp(argv[1], "*") == 0) dst...`
- `if (strcmp(argv[2], "*") == 0) dst...`
- `if (strcmp(argv[3], "*") == 0 dst...`

mengecek apakah paramater jam, menit, dan detik sama dengan karakter '*'. Jika iya, maka variabel hour, minute, atau second diubah menjadi -1 sebagai nilai penanda.

misalnya, jika argumen pertama adalah *, artinya tidak ada batasan untuk jam yang diberikan. Dalam hal ini, variabel hour akan diatur menjadi -1 yang akan mengindikasikan bahwa semua nilai jam akan diterima. 


3. Berikut kode untuk mengatur waktu sleep pertama kali.
```
int firstSleep = 0;

  if (hour == -1) {
    if (minute == -1) {
      if (second >= 0) {
        if (second < curr_s) {
          firstSleep = (second + 60) - curr_s;
        } else {
          firstSleep = second - curr_s;
        }
      } else {
        firstSleep = 1;
      }
    } else {
      firstSleep = (((minute * 60) + second) - ((curr_m * 60) + curr_s));
      if (minute < curr_m || (minute == curr_m && second < curr_s)) {
        firstSleep += 3600;
      }
    }
  } else {
    firstSleep = (((hour * 3600) + (minute * 60) + second) - ((curr_h * 3600) + (curr_m * 60) + curr_s));
    if (hour < curr_h || (hour == curr_h && (minute < curr_m || (minute == curr_m && second < curr_s)))) {
      firstSleep += 86400;
    }
  }

  if (hour == -1 && minute == -1 && second == -1) {
    firstSleep = 1;
  }

  bool first = true;

```
- Kode di atas menghitung berapa lama program harus menunggu sebelum pertama kali dieksekusi. Waktu tunggu ini dihitung berdasarkan waktu saat ini dan waktu yang ditentukan oleh argumen program.

- Jika argumen hour sama dengan -1, artinya tidak ada batasan untuk jam yang diberikan, dan jika argumen minute sama dengan -1, artinya tidak ada batasan untuk menit yang diberikan. Dalam hal ini, waktu tunggu akan dihitung berdasarkan nilai argumen second. Jika second lebih besar dari atau sama dengan curr_s (nilai detik saat ini), maka waktu tunggu akan dihitung sebagai selisih antara second dan curr_s. Namun, jika second lebih kecil dari curr_s, artinya waktu tunggu harus dihitung sebagai selisih antara second dan curr_s, ditambah 60 (jumlah detik dalam satu menit) untuk mengkompensasi detik yang hilang. Jika argumen second sama dengan -1, artinya tidak ada batasan untuk detik yang diberikan, maka program akan menunggu selama satu detik sebelum dieksekusi pertama kali.

- Jika argumen hour tidak sama dengan -1, artinya program harus dieksekusi pada waktu tertentu dalam satu hari. Dalam hal ini, waktu tunggu dihitung sebagai selisih antara waktu yang ditentukan oleh argumen dan waktu saat ini, diukur dalam detik. Jika waktu yang ditentukan sudah lewat, artinya program harus menunggu hingga keesokan harinya untuk dieksekusi pertama kali. Oleh karena itu, waktu tunggu akan ditambahkan dengan jumlah detik dalam satu hari (86400 detik) jika waktu yang ditentukan sudah lewat.

- Jika semua argumen sama dengan -1, artinya tidak ada batasan untuk waktu yang diberikan, maka program akan menunggu selama satu detik sebelum dieksekusi pertama kali.

- `bool first = true` digunakan untuk mengecek apakah program sleep sudah pernah berjalan atau belum, jika sudah maka variabel first diubah menjadi false.

4. Berikut adalah kode implementasi template daemon
```
pid_t pid = fork();
  if (pid < 0) {
    exit(EXIT_FAILURE);
  }

  if (pid > 0) {
    exit(EXIT_SUCCESS);
  }

  if (setsid() < 0) {
    exit(EXIT_FAILURE);
  }

  close(STDIN_FILENO);
  close(STDOUT_FILENO);
  close(STDERR_FILENO);

```

5. Berikut adalah kode utama untuk menjalankan program seperti crontab
```
while (1) {

    int sleepTime = 0;
    if (hour == -1 && minute == -1 && second == -1) {
      sleepTime = 1;
    }
    if (hour != -1) {
      sleepTime += 86400;
    } else if (minute != -1) {
      sleepTime += 3600;
    } else if (second != -1) {
      sleepTime += 60;
    }

    if (first) {
      sleep(firstSleep);
      first = false;

      if (hour == -1) {
        if (minute == -1) {
          if (second == -1) {
            sleepTime = 1;
          }
        } else {
          if (second == -1) {
            sleepTime -= curr_s;
          }
        }

      } else {
        if (minute == -1 && second == -1) {
          sleepTime -= ((curr_m * 60) + curr_s);
        } else if (minute == -1 && second >= 0) {
          sleepTime -= (curr_m * 60);
        } else if (minute >= 0 && second == -1) {
          sleepTime -= curr_s;
        }

      }

    }

    pid_t cid = fork();

    if (cid == -1) {
      printf("Error : failed to fork proccess\n");
    } else if (cid == 0) {
      char * args[] = {
        "bash",
        argv[4],
        NULL
      };
      execv("/bin/bash", args);
    } else {
      wait(NULL);
    }

    sleep(sleepTime);

  }

  return 0;


```
- Pertama, loop ini akan menghitung berapa lama waktu tidur yang dibutuhkan sebelum menjalankan perintah bash. Jika argumen hour, minute, dan second semuanya sama dengan -1, artinya tidak ada interval waktu yang ditentukan, maka program akan tidur selama 1 detik. Jika hour bukan -1, artinya interval waktu yang ditentukan lebih dari atau sama dengan 1 jam, maka program akan tidur selama 86400 detik (1 hari). Jika minute bukan -1, artinya interval waktu yang ditentukan lebih dari atau sama dengan 1 menit, maka program akan tidur selama 3600 detik (1 jam). Jika second bukan -1, artinya interval waktu yang ditentukan lebih dari atau sama dengan 1 detik, maka program akan tidur selama 60 detik (1 menit).

- Variabel first digunakan untuk menandai apakah ini adalah loop pertama atau tidak. Jika first bernilai true, maka program akan tidur selama firstSleep detik sebelum menjalankan perintah bash. firstSleep dihitung dengan menghitung waktu tidur dari saat loop pertama kali dijalankan hingga waktu yang ditentukan. Jika hour sama dengan -1, maka firstSleep dihitung dari menit dan detik yang ditentukan. Jika minute sama dengan -1 dan second tidak sama dengan -1, maka firstSleep dihitung dari detik yang ditentukan. Jika minute tidak sama dengan -1 dan second sama dengan -1, maka firstSleep dihitung dari menit yang ditentukan.

- Setelah menentukan waktu tidur dan menunggu selama waktu yang ditentukan, program akan melakukan fork() untuk membuat child process yang akan menjalankan perintah bash. Jika fork() gagal, maka program akan mencetak pesan kesalahan. Jika fork() berhasil, maka child process akan menggunakan execv() untuk menjalankan perintah bash. Jika child process berhasil dijalankan, maka parent process akan menunggu hingga child process selesai menjalankan perintah bash dengan menggunakan wait().

- Setelah menunggu child process selesai, loop akan mengulang dan program akan tidur selama waktu yang telah dihitung sebelumnya sebelum menjalankan perintah bash lagi. Loop ini akan terus berjalan sampai program dihentikan.

 ![WhatsApp Image 2023-04-08 at 21 43 52](https://user-images.githubusercontent.com/88433109/230727543-a77e219a-684a-4a61-a936-33d18fe3e9e4.jpg)

 ![WhatsApp Image 2023-04-08 at 21 45 27](https://user-images.githubusercontent.com/88433109/230727628-6d7c0079-ce72-4c19-8b17-9b5493d42d70.jpg)
