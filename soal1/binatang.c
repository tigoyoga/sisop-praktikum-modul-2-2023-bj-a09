#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <string.h>

void makeDirectories();
void moveFiles();
void compressFiles();
void clearProcesses();
void downloadFiles();
void shiftAnimal();

int main()
{
    downloadFiles();
    shiftAnimal();
    makeDirectories();
    moveFiles();
    compressFiles();
    clearProcesses();
    return 0;
}

void makeDirectories()
{
    pid_t pid;
    pid = fork();
    if(pid==0){
        char *argv[] = {"mkdir", "-p", "HewanDarat", "HewanAir", "HewanAmphibi", NULL};
        execv("/bin/mkdir", argv);
        exit(0);
    }else if(pid>0){
        wait(NULL);
        printf("all directory success created\n");
    }
}

void shiftAnimal()
{
    char command[256];
    snprintf(command, sizeof(command), "/usr/bin/shuf -n 1 *.jpg");
    system(command);
}

void moveFiles(){
    system("mv *air.jpg HewanAir");
    system("mv *darat.jpg HewanDarat");
    system("mv *amphibi.jpg HewanAmphibi");
}

void compressFiles()
{
    pid_t pid;
    pid = fork();
    if(pid==0)
    {
        char *argv[] = {"zip","-r", "HewanAir.zip", "HewanAir", NULL};
        execv("/bin/zip", argv);
        exit(0);
    }
    else if(pid>0)
    {
        wait(NULL);
        pid = fork();
        if (pid==0)
        {
            char *argv[] = {"zip", "-r", "HewanDarat.zip", "HewanDarat", NULL};
            execv("/bin/zip", argv);
            exit(0);
        }
        else if(pid>0)
        {
            wait(NULL);
            pid = fork();
            if (pid == 0)
            {
                char *argv[] = {"zip", "-r", "HewanAmphibi.zip", "HewanAmphibi", NULL};
                execv("/bin/zip", argv);
                exit(0);
            } else if(pid>0){
                wait(NULL);
                printf("zip all directories success\n");
            }
        }
    }
}

void clearProcesses()
{
    system("rm -rf HewanDarat");
    system("rm -rf HewanAir");
    system("rm -rf HewanAmphibi");
    printf("all process are cleared!");
}

void downloadFiles()
{
    pid_t pid ;

    pid = fork();

    if(pid==0){
        char *argv[] = {"wget", "https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq", "-O", "binatang.zip", NULL};
        execv("/usr/bin/wget", argv);
        exit(0);
    }else if(pid>0){
        wait(NULL);

        pid = fork();
        if(pid==0){
            char *argv[] = {"unzip", "binatang.zip", NULL};
            execv("/bin/unzip", argv);
            exit(0);
        }else if(pid>0){
            wait(NULL);
            printf("DOWNLOAD AND UNZIPPP SUCCESS\n");
        }
    }
    
    
}