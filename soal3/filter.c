#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <dirent.h>
#include <string.h>
#define BUFFER_SIZE 1536


void donlod();
void unzip_delete();
void hapusNonMU();
void seleksiPosisi();
void buatTim(int, int, int);
void rapihinTeks(int, int, int);
// void seleksiPosisi();
// void buatTim();a


int main() {
    // tentukan formasi
    int formasi[3] = {4,3,3}; //{bek, gelandang, penyerang}

    pid_t pid1 = fork();

    if(pid1 == 0){
        donlod();
        unzip_delete();
        exit(0);
    } else if(pid1>0){
        wait(NULL);
        hapusNonMU();
        seleksiPosisi();
        buatTim(formasi[0], formasi[1], formasi[2]);
        rapihinTeks(formasi[0], formasi[1], formasi[2]);
        printf("NON MU SUDAH TERHAPUSSSSS\n");
    }
    
    return 0;
}


void donlod(){
    int stat;
    pid_t pid1 = fork();

    if(pid1 == 0){
        execl("/usr/bin/wget", "wget", "-O", "players.zip", "https://drive.google.com/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download", NULL);
    }
    while(wait(&stat) > 0) ;
    waitpid(pid1, &stat, 0);
}

void unzip_delete(){
    // int stat;
    pid_t pid1 = fork();

    if(pid1 == 0){
        execl("/usr/bin/unzip", "unzip", "players.zip", NULL);
        exit(0);
    } 
    else if (pid1 > 0){
        wait(NULL);
        execl("/bin/rm", "rm", "players.zip", NULL);
        // printf("File selection based on position has been completed\n");
    }
    

}

void hapusNonMU(){
    int pipefd[2];
    pid_t child_pid;
    char buffer[BUFFER_SIZE];

    // membuat pipe untuk menghubungkan parent dan child process
    if (pipe(pipefd) == -1) {
        perror("pipe");
        exit(EXIT_FAILURE);
    }

    // membuat child process
    child_pid = fork();

    if (child_pid == -1) {
        perror("fork");
        exit(EXIT_FAILURE);
    }

    if (child_pid == 0) {
        // child process
        // tutup file descriptor yang tidak digunakan oleh child process
        close(pipefd[0]);

        // redirect stdout ke pipe
        dup2(pipefd[1], STDOUT_FILENO);

        // jalankan command ls
        char *args[] = {"ls", "-1", "players", NULL};
        execv("/bin/ls", args);
    } else {
        // parent process
        // tutup file descriptor yang tidak digunakan oleh parent process
        close(pipefd[1]);

        // baca output dari child process menggunakan pipe
        ssize_t count = read(pipefd[0], buffer, BUFFER_SIZE);

        // menunggu child process selesai dijalankan
        waitpid(child_pid, NULL, 0);

        // menampilkan daftar file di console
        printf("Daftar file di dalam folder players:\n");
        printf("%.*s", (int)count, buffer);

        // iterasi pada daftar file dan hapus file yang tidak memiliki nama "ManCity"
        char* filename;
        filename = strtok(buffer, "\n");
        while (filename != NULL) {
            if (!strstr(filename, "ManUtd")) {
                char filepath[BUFFER_SIZE];
                snprintf(filepath, BUFFER_SIZE, "players/%s", filename);
                if (access(filepath, F_OK) != -1 ){
                    if (remove(filepath) == 0) {
                        printf("File '%s' telah dihapus.\n", filename);
                    } else {
                        printf("Gagal menghapus file '%s'.\n", filename);
                    }
                } else{
                     printf("Program tidak memiliki hak akses untuk menghapus file '%s'.\n", filename);
                }
                
            }
            filename = strtok(NULL, "\n");
        }
    }
}

void seleksiPosisi(){
    pid_t pid;

    pid = fork();
    if (pid == 0){
        char *args[] = {"mkdir", "Kiper", NULL};
        execvp(args[0], args);
        printf("Failed to execute mkdir kiper command\n");
    }
    else if (pid > 0){
        wait(NULL);

        pid = fork();
        if (pid == 0){
            char *args2[] = {"find", "players", "-type", "f", "-name", "*Kiper*", "-exec", "mv", "{}", "Kiper/", ";", NULL};
            execvp(args2[0], args2);
            printf("Failed to execute find Kiper command\n");
        }
        else if (pid > 0){
            wait(NULL);

            pid = fork();
            if (pid == 0){
                char *args[] = {"mkdir", "Bek", NULL};
                execvp(args[0], args);
                printf("Failed to execute mkdir Bek command\n");
            }
            else if (pid > 0){
                wait(NULL);

                pid = fork();
                if (pid == 0){
                    char *args3[] = {"find", "players", "-type", "f", "-name", "*Bek*", "-exec", "mv", "{}", "Bek/", ";", NULL};
                    execvp(args3[0], args3);
                    printf("Failed to execute find bek command\n");
                }
                else if (pid > 0){
                    wait(NULL);

                    pid = fork();
                    if (pid == 0){
                        char *args[] = {"mkdir", "Gelandang", NULL};
                        execvp(args[0], args);
                        printf("Failed to execute mkdir Gelandang command\n");
                    }
                    else if (pid > 0){
                        wait(NULL);

                        pid = fork();
                        if (pid == 0){
                            char *args4[] = {"find", "players", "-type", "f", "-name", "*Gelandang*", "-exec", "mv", "{}", "Gelandang/", ";", NULL};
                            execvp(args4[0], args4);
                            printf("Failed to execute find Gelandang command\n");
                        }
                        else if (pid > 0){
                            wait(NULL);

                            pid = fork();
                            if (pid == 0){
                                char *args[] = {"mkdir", "Penyerang", NULL};
                                execvp(args[0], args);
                                printf("Failed to execute mkdir Penyerang command\n");
                            }
                            else if (pid > 0){
                                wait(NULL);

                                pid = fork();
                                if (pid == 0){
                                    char *args5[] = {"find", "players", "-type", "f", "-name", "*Penyerang*", "-exec", "mv", "{}", "Penyerang/", ";", NULL};
                                    execvp(args5[0], args5);
                                    printf("Failed to execute find Penyerang command\n");
                                }
                                else if (pid > 0){
                                    wait(NULL);
                                    printf("File selection based on position has been completed\n");
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

void buatTim(int jum_bek, int jum_gelandang, int jum_penyerang){

    pid_t pid;
    // char fileName[30];
    // sprintf(fileName, "Formasi_%d_%d_%d.txt", jum_bek, jum_gelandang, jum_penyerang);

    pid = fork();

    if(pid == 0){
        //kiper
        char command[120];

        // jalankan perintah untuk mencari 3 nilai tertinggi dalam folder "bek"
        sprintf(command, "ls Kiper | sort -t_ -k4 -nr | head -n %d | awk -F_ '{print $1\"_\"$2\"_\"$3\"_\"$4}' | xargs >> Formasi_%d_%d_%d.txt", 1, jum_bek, jum_gelandang, jum_penyerang);
        execlp("sh", "sh", "-c", command, NULL);

        // jika exec berhasil, kode di bawah ini tidak akan dieksekusi
        perror("exec failed");
        exit(EXIT_FAILURE);
    }else if(pid > 0){
        wait(NULL);

        pid = fork();
        if(pid == 0){
            //bek

            char command[120];

            // jalankan perintah untuk mencari 3 nilai tertinggi dalam folder "bek"
            sprintf(command, "ls Bek | sort -t_ -k4 -nr | head -n %d | awk -F_ '{print $1\"_\"$2\"_\"$3\"_\"$4}' | xargs >> Formasi_%d_%d_%d.txt", jum_bek, jum_bek, jum_gelandang, jum_penyerang);
            execlp("sh", "sh", "-c", command, NULL);

            // jika exec berhasil, kode di bawah ini tidak akan dieksekusi
            perror("exec failed");
            exit(EXIT_FAILURE);
        }else if(pid>0){
            wait(NULL);

            pid = fork();
            if(pid == 0){
                //gelandang
                char command[120];

                // jalankan perintah untuk mencari 3 nilai tertinggi dalam folder "bek"
                sprintf(command, "ls Gelandang | sort -t_ -k4 -nr | head -n %d | awk -F_ '{print $1\"_\"$2\"_\"$3\"_\"$4}' | xargs >> Formasi_%d_%d_%d.txt", jum_gelandang, jum_bek, jum_gelandang, jum_penyerang);
                execlp("sh", "sh", "-c", command, NULL);

                // jika exec berhasil, kode di bawah ini tidak akan dieksekusi
                perror("exec failed");
                exit(EXIT_FAILURE);
            }else if(pid>0){
                wait(NULL);

                pid = fork();
                if(pid==0){
                    //penyerang
                    char command[120];

                    // jalankan perintah untuk mencari 3 nilai tertinggi dalam folder "bek"
                    sprintf(command, "ls Penyerang | sort -t_ -k4 -nr | head -n %d | awk -F_ '{print $1\"_\"$2\"_\"$3\"_\"$4}' | xargs >> Formasi_%d_%d_%d.txt", jum_penyerang, jum_bek, jum_gelandang, jum_penyerang);
                    execlp("sh", "sh", "-c", command, NULL);

                    // jika exec berhasil, kode di bawah ini tidak akan dieksekusi
                    perror("exec failed");
                    exit(EXIT_FAILURE);
                }else if(pid>0){
                    // wait for child process to finish
                    wait(NULL);
                    printf("suksesssss\n");

                }
            }
        }
    }
}


void rapihinTeks(int jum_bek, int jum_gelandang, int jum_penyerang){
    char command[120];
    pid_t pid;
    pid=fork();
    if(pid==0){
        sprintf(command, "tr ' ' '\n' < Formasi_%d_%d_%d.txt >tmp.txt && mv tmp.txt Formasi_%d_%d_%d.txt",jum_bek, jum_gelandang, jum_penyerang, jum_bek, jum_gelandang, jum_penyerang );
        execlp("sh", "sh", "-c", command, NULL);
    }else if(pid>0){
        wait(NULL);
    }
}